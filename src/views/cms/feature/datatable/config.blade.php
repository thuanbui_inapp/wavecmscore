<div class="card-header card-header-config ">
	<div class="table-search">
		<label><input type="search" class="form-control input-sm" placeholder="Search..." aria-controls="DataTables_Table_0"></label>
	</div>
	<form class="table-setting-xlsx-form" method="post" target="_blank" style="width:0px;display:inline-block"></form>
	<form class="table-setting-csv-form" method="post" target="_blank" style="width:0px;display:inline-block"></form>
	<div class="table-button">
		{{--<div class="btn table-setting btn-success btn-xs table-setting-filter" tooltip="Advanced Filter" data-original-title="" title=""><i class="icon fa fa-filter"></i></div>
		<div class="btn table-setting btn-success btn-xs table-setting-column" tooltip="Custom Column" data-original-title="" title=""><i class="icon fa fa-th-list"></i></div>--}}
		<div class="btn table-setting btn-success btn-xs table-setting-xlsx" tooltip="Download Excel" data-original-title="" title=""><i class="icon fa fa-file-excel-o"></i></div>

		<div class="btn table-setting btn-success btn-xs table-setting-csv" tooltip="Download CSV" data-original-title="" title=""><i class="icon fa fa-file-text-o"></i></div>
		{{--<div class="btn table-setting btn-success btn-xs table-setting-setting" tooltip="Setting" data-original-title="" title=""><i class="icon fa fa-gear"></i></div>--}}
	</div>
</div>