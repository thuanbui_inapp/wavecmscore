<div class="card chart-card">
	<div class="card-header card-header-add">
		Top 10 PageVisit
		<ul class="nav nav-tabs pull-right">
			@foreach(generatePastDays(7, 14, 30) as $key => $value)
				<li @if($key === 0) class="active" @endif>
					<a href="#tab-pagevisit-{{$value}}" data-toggle="tab">
						Last {{ $value }} Days
					</a>
				</li>
			@endforeach
		</ul>
	</div>
	<div class="card-body no-padding">
		<div class="tab-content">
			@foreach(generatePastDays(7, 14, 30) as $key => $value)
				<div id="tab-pagevisit-{{$value}}" class="tab-pane fade @if($key === 0) in active @endif" data-toggle="tab">
					<table class="table table-hover">
						<thead>
						<tr>
							<th>Page</th>
							<th>Visits</th>
						</tr>
						</thead>
						<tbody>
						@foreach(\App\Service\GoogleAnalyticService::GetPageVisit($value.'daysAgo', 'today', 10) as $items)
							<tr>
								<td>{{ $items[0] }}</td>
								<td>{{ $items[1] }}</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			@endforeach
		</div>
	</div>
</div>
