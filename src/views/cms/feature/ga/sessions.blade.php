<div class="card chart-card">
    <div class="card-header">
        Sessions
        <ul class="nav nav-tabs pull-right">
            @foreach(generatePastDays(7, 14, 30) as $key => $value)
                <li @if($key === 0) class="active" @endif>
                    <a href="#tab-session-{{$value}}" data-toggle="tab">
                        Last {{ $value }} Days
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
    <div class="card-body">
        <div class="tab-content">
            @foreach(generatePastDays(7, 14, 30) as $key => $value)
                <div id="tab-session-{{$value}}" class="tab-pane fade @if($key === 0) in active @endif" data-toggle="tab">
                    <canvas id="{{ $value }}day-gasessions"></canvas>
                </div>
            @endforeach
        </div>
    </div>
</div>

@section('jsCustom')
    <script>
        @foreach(generatePastDays(7, 14, 30) as $key => $value)
        GenerateLineChart(
            '#'+'{!! $value !!}'+'day-gasessions',
            '/admin/ga/ajax/sessions',
                {!! json_encode(getLastNdaysDates($value)) !!},
            ['Sessions'],
            undefined,
            {
                'dateFrom': '{!! $value !!}'+'daysAgo'
            }
        );
        @endforeach
    </script>
@append