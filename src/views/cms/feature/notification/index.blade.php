@extends('cms.layouts.authorized')

@section('title', 'Notification')

@section('headerCustom')
    @php
        $title = $model::getTitleIndex();
    @endphp
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-body no-padding">
                    <table class="table primary" id="notification-table">
                        <thead>
                        <tr>
                            <th>Notifikasi</th>
                        </tr>
                        </thead>
                        <tbody class="notification-list">
                        @foreach($list as $item)
                            <tr class="notification-item">
                                <td>
                                    <span class="hidden">{!! strtotime($item->notificationDate) !!}</span>
                                    <a href="{{ route('admin.notification.my.topic', ['id' => $item->id]) }}">
                                        <div class="notification-time">
                                            {!! elapsedTime($item->notificationDate) !!}
                                        </div>
                                        <div class="notification-text">
                                            {!! $item->message !!}
                                        </div>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jsCustom')
    <script>
        $('#notification-table').DataTable({
            dom: 'rt<"bottom"p<"clear">>',
            order: [[0, "desc"]],
            columnDefs: [
                { targets: 0, orderable: false}
            ]
        });
    </script>
@endsection