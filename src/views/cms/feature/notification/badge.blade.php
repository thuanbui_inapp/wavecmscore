@php
	$notificationCount = \App\Service\CMSCore\NotificationService::MyUnreadNotificationCount();
@endphp

<li class="mr-15 dropdown notification hidden-sm">
	<div class="dropdown-toggle"  data-toggle="dropdown">
		<div class="avatar-icon-counter @if($notificationCount == 0) hidden @endif ">
			{{ $notificationCount }}
		</div>
		<div class="avatar-icon">
			<i class="fa fa-bell @if($notificationCount > 0) faa-shake animated @endif" aria-hidden="true"></i>
		</div>
	</div>
	<div class="dropdown-menu ">
		<div class="header">
			<div class="title">Alert</div>
			<div class="markall @if($notificationCount == 0) hidden @endif">Mark all as read</div>
		</div>
		<ul class="action">
			@if($notificationCount > 0)
				@foreach(range(1, ($notificationCount > 5 ? 5 : $notificationCount) ) as $loop)
					<li>
						<div class="demo"></div>
					</li>
				@endforeach
			@else
				<li class="empty">
					There is no New Notification
				</li>
			@endif
			<li class="all">
				See All Unread Notification
			</li>
		</ul>
	</div>
</li>
