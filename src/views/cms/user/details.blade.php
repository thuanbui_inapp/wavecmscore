@extends('cms.layouts.authorized')

@section('headerCustom')
	@php
		$id = 0;
		if (!empty($model) && !empty($model->getKey())) $id = $model->getKey();

		if(empty($id)) $title = getPrefixTitleDetails() . ' ' . $model::getTitleDetails();
		else $title = $model::getTitleDetails();

		$formUrl = $model->getUrlDetails(['id' => $id]);
		$cancelUrl = $model->getUrlIndex();

		$language = '';

	@endphp
@endsection

@include('cms.form.details')

@section('jsCustom')

	@if (empty($model->id))
		<script>
            $(document).ready(function(){
                formRequired.push('password');
            });
		</script>
	@endif

@append

