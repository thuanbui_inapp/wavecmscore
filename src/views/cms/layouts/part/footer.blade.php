<footer class="app-footer">
    <div class="row">
        <div class="col-xs-12">
            <div class="footer-copyright">
                <br/>Copyright © {{ empty(config('cms.COPYRIGHT_YEAR')) ? date('Y') : config('cms.COPYRIGHT_YEAR') }} {{ env('PROJECT_NAME') }}
            </div>
        </div>
    </div>
</footer>