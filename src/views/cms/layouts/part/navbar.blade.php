<nav class="navbar navbar-default" id="navbar">
    <div class="container-fluid">
        <div class="navbar-collapse collapse in">
            <ul class="nav navbar-nav navbar-mobile">
                <li>
                    <button type="button" class="sidebar-toggle">
                        <i class="fa fa-bars"></i>
                    </button>
                </li>
                <li class="logo">
                    <a class="navbar-brand" href="#">{{ env('PROJECT_NAME') }}</a>
                </li>
                <li>
                    <button type="button" class="navbar-toggle">
                        <div class="avatar-icon">
                            <i class="avatar-icon fa fa-user" aria-hidden="true"></i>
                        </div>
                    </button>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-left">
                <li class="navbar-title">{{ @$title }}</li>
                @if(isset($model) && count(@\Illuminate\Support\Facades\Config::get('cms.LANGUAGE'))>1)
                    @foreach(@\Illuminate\Support\Facades\Config::get('cms.LANGUAGE') as $index=>$language)
                        <div>
                            <img class="lang-icon @if($index==0) active @endif" language="{{$language}}" src="{{ url('/assets/wvi/cmscore/image/lang/'.@$language).'.png' }}">
                        </div>
                    @endforeach
                @endif
            </ul>
            <ul class="nav navbar-nav navbar-right">
                @if (config('cms.notification') === true) @notificationbadge @endif

                <li>
                    <div class="text-right">
                        <div class="username">{{ \Auth::user()->name }}</div>
                        <div class="email">{{ \Auth::user()->email }}</div>
                    </div>
                </li>
                <li class="dropdown hidden-sm">
                    <div class="dropdown-toggle"  data-toggle="dropdown">
                        <div class="avatar-icon">
                            <i class="fa fa-user" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="dropdown-menu">
                        <div class="profile-info">
                            <h4 class="username">{{ \Auth::user()->name }}</h4>
                        </div>
                        <ul class="action">
                            <li>
                                <a href="{{ empty(config('cms.MYPROFILE_URL')) ? url('/admin/user') : config('cms.MYPROFILE_URL') }}">Profile</a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/logout') }}">Logout</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="dropdown profile hidden-md hidden-lg">
                    <a href="{{ url('/admin/user') }}">
                        <div class="title">Profile</div>
                    </a>
                </li>
                <li class="dropdown profile hidden-md hidden-lg">
                    <a href="{{ url('/admin/logout') }}">
                        <div class="title">Logout</div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>