<script>
	var datatable = {};
    var datatablePerPage = {!! empty(\Illuminate\Support\Facades\Config::get('cms.DATATABLE_PERPAGE')) ? 10 : \Illuminate\Support\Facades\Config::get('cms.DATATABLE_PERPAGE')  !!};
</script>

@js('/assets/wvi/cmscore/js/theme/vendor.js')
@js('/assets/wvi/cmscore/js/lib/select2.full.min.js')

@js('/assets/wvi/cmscore/js/theme/app.js')

@js('/assets/wvi/cmscore/js/lib/moment.min.js')
@js('/assets/wvi/cmscore/js/lib/bootstrap-datetimepicker.min.js')

@js('/assets/wvi/cmscore/js/lib/autoNumeric.min.js')
@js('/assets/wvi/cmscore/js/lib/lobipanel.min.js')
@js('/assets/wvi/cmscore/js/lib/sweetalert.min.js')
@js('/assets/wvi/cmscore/js/lib/toastr.min.js')
@js('/assets/wvi/cmscore/js/lib/chart.js')
@js('/assets/wvi/cmscore/js/lib/chart.util.js')
@js('/assets/wvi/cmscore/js/lib/fastselect.standalone.min.js')
@js('/assets/wvi/cmscore/js/lib/summernote.min.js')
@js('/assets/wvi/cmscore/js/lib/iziModal.js')

@js('/assets/wvi/cmscore/library/ladda/js/spin.min.js')
@js('/assets/wvi/cmscore/library/ladda/js/ladda.min.js')
@js('/assets/wvi/cmscore/library/ladda/js/ladda.jquery.min.js')

@js('/assets/wvi/cmscore/js/util.js')
@js('/assets/wvi/cmscore/js/form-engine.js')
@js('/assets/wvi/cmscore/js/form-engine.autocomplete.js')
@if(config('cms.notification') === true) @js('/assets/wvi/cmscore/js/notification.js') @endif
@js('/assets/wvi/cmscore/js/datatable.js')
@js('/assets/wvi/cmscore/js/chart.js')

@if(isset($isVue))
	<script>
		var defaultMoney = {precision: 0, thousands: ".", decimal: ","};
	</script>
	@js('/assets/wvi/cmscore/js/lib/vue.min.js')
	@js('/assets/wvi/cmscore/js/lib/vue-the-mask.js')
	@js('/assets/wvi/cmscore/js/lib/vue-money.min.js')

	@js('/assets/wvi/cmscore/js/form-engine.vue.select2.js')
	@js('/assets/wvi/cmscore/js/form-engine.vue.image.js')
	@js('/assets/wvi/cmscore/js/form-engine.vue.autonumeric.js')
	@js('/assets/wvi/cmscore/js/form-engine.vue.date.js')

	@js('/assets/wvi/cmscore/js/form-engine.vue.js')
@endif

@if(!empty($model))
	<script>
		var formRequired = {!! json_encode(getFormRequiredArray($model)) !!};
		var formImage = {!! json_encode(getFormImageArray($model)) !!};
		var languangeLabel = {!! json_encode(languangeLabel($model)) !!};
	</script>
@endif

