<script>
	var uploadImageUrl = '{!! url(env('UPLOAD_IMAGE')) !!}';
</script>

@css('/assets/wvi/cmscore/css/theme/vendor.css')
@css('/assets/wvi/cmscore/css/theme/flat-admin.css')
@css('/assets/wvi/cmscore/css/theme/blue.css')
@css('/assets/wvi/cmscore/css/theme/blue-sky.css')
@css('/assets/wvi/cmscore/css/theme/default.css')
@css('/assets/wvi/cmscore/css/theme/red.css')
@css('/assets/wvi/cmscore/css/theme/yellow.css')

@css('/assets/wvi/cmscore/css/lib/select2.min.css')

@css('/assets/wvi/cmscore/css/lib/bootstrap-datetimepicker.min.css')
@css('/assets/wvi/cmscore/css/lib/custom-bootstrap-margin-padding.css')
@css('/assets/wvi/cmscore/css/lib/jquery-ui.min.css')
@css('/assets/wvi/cmscore/css/lib/lobipanel.min.css')
@css('/assets/wvi/cmscore/css/lib/sweetalert.css')
@css('/assets/wvi/cmscore/css/lib/toastr.min.css')
@css('/assets/wvi/cmscore/css/lib/fastselect.min.css')
@css('/assets/wvi/cmscore/css/lib/summernote.css')
@css('/assets/wvi/cmscore/css/lib/icomoon.css')
@css('/assets/wvi/cmscore/css/lib/iziModal.min.css')

@css('/assets/wvi/cmscore/library/ladda/css/ladda-themeless.min.css')

@css('/assets/wvi/cmscore/css/style.css')
@css('/assets/admin/css/style.css')

@if(isset($isVue))
	@css('/assets/wvi/cmscore/css/lib/vue2-animate.css')
@endif