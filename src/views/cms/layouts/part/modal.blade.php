<!-- Modal -->
<div id="izimodal" aria-hidden="true" aria-labelledby="izimodal" role="dialog" class="iziModal" style="z-index: 999; border-radius: 3px; border-bottom: 3px solid rgb(189, 91, 91); overflow: hidden; max-width: 600px;">
</div>

<div class="modal fade" id="reset-password-modal-confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">User Invited</h4>
            </div>
            <div class="modal-body">
                We have successfully send your new password through email! <br/>
            </div>
            <div class="modal-footer">
                <a href="{{ url('/admin/login') }}">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </a>

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-confirm-action" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">User Invited</h4>
            </div>
            <div class="modal-body">
                We have successfully send your new password through email! <br/>
            </div>
            <div class="modal-footer">
                <a href="{{ url('/admin/login') }}">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </a>

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-delete-alert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center" id="myModalLabel">CONFIRM DELETE</h4>
            </div>
            <div class="modal-body text-center">
                Please confirm to delete this Item. Once deleted, you must re-create this Item if needed. <br/>
            </div>
            <div class="modal-footer">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <a>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </a>
                    </div>
                    <div class="col-md-6 text-left">
                        <a>
                            <button type="button" class="btn btn-default btn-danger delete-button-confirm" >Delete</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-display-image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header p-15" id="modalDisplayImageLabel">
            </div>
            <div class="modal-body p-15 text-center">
                <img src="" alt="" class="img-thumbnail" id="modalDisplayImageSource">
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <a download="" href="" class="btn btn-xs btn-primary" id="modalDisplayImageDownload">
                            <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> Download Image
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>