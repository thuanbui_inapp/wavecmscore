<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex">

    <title>{{ env('PROJECT_NAME') }} - @yield('title')</title>

    @include('cms.layouts.part.css')

    @yield('cssCustom')

</head>

<body id="app-layout">
    @include('admin.cmscore.part.sidebar')

    @yield('headerCustom')

    <div class="app-container">
        @include('cms.layouts.part.navbar')

        @if(isset($isVue))<div id="vueApp">@endif
            @yield('content')
        @if(isset($isVue))</div>@endif

        @include('cms.layouts.part.footer')

    </div>

    @yield('modal')

    @include('cms.layouts.part.modal')
    @include('cms.layouts.part.js')

    @yield('jsCustom')

    @if(session()->has('success'))
        <script>
            var successMessage = '{!! session()->has('success') ? session('success')  : null !!}';

            swal('Success!', successMessage, 'success');
        </script>
    @elseif(session()->has('error'))
        <script>
            var errorMessage = '{!! session()->has('error') ? session('error')  : null !!}';

            swal('Error!', errorMessage, 'error');
        </script>
    @endif

</body>
</html>
