<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ env('PROJECT_NAME') }}</title>

    @include('cms.layouts.part.css')

    @yield('headerCustom')
</head>

<body>
    @yield('content')

    @yield('jsCustom')
</body>
</html>
