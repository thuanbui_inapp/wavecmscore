@extends('cms.layouts.authorized')

@section('headerCustom')
    @php
    $id = 0;
    if (!empty($model->getKey())) $id = $model->getKey();

    if($typeEntity::ALLOW_MULTIPLE) {
    $title = 'new ' . $subtype;
    $formUrl = url('/admin/cms/details/'.$type.'/'.$subtype.'/'.$id);
    $cancelUrl = url('/admin/cms/'.$type.'/'.$subtype);
    }
    else {
    $id = '';
    if (!empty($model::CMS_NAME)) $title = $model::CMS_NAME;
    else $title = $model->name;
    $formUrl = url('/admin/cms/details/'.$type.'/'.$subtype);
    $cancelUrl = url('/admin/cms/'.$type);
    }
    $language = '';

    @endphp
@endsection

@section('button')

    @include('cms.form.button')

@endsection

@section('content')

    @include('cms.form.errorbox')

    <form id="form" action="{{ $formUrl }}" class="create-project form-horizontal" method="POST"  enctype="multipart/form-data">

        @if(count(\Illuminate\Support\Facades\Config::get('cms.LANGUAGE'))>1)
            @foreach(\Illuminate\Support\Facades\Config::get('cms.LANGUAGE') as $index=>$language)
                <div class="languageSection languageSection{{ $language }}" @if($index != 0) style="display:none;" @endif>
                    @include('cms.form.main')
                </div>
            @endforeach
        @else
            @include('cms.form.main')
        @endif

    </form>

@endsection

@if(View::exists('admin.'.strtolower($subtype).'.detailsCustom'))
    @include('admin.'.strtolower($subtype).'.detailsCustom')
@endif