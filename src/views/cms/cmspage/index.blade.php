@extends('cms.layouts.authorized')

@section('title', 'Page')

@section('headerCustom')
    @php
    if (!empty($subtype)) {
    $entity = nameToEntity($subtype);
    $title = $entity::CMS_NAME;
    }
    else $title = $type;
    @endphp
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header @if($typeEntity::ALLOW_MULTIPLE )card-header-add @endif">
                    @if($typeEntity::ALLOW_MULTIPLE )
                        <span>{{ $subtype }}</span>
                        <a type="button" class="btn btn-success btn-xs pull-right" href=" {{ url('/admin/cms/details/'.$type.'/'.$subtype.'/0') }}">
                            <i class="icon fa fa-plus"></i>
                            <span class="help-text">Create new {{ $subtype }}</span>
                        </a>
                    @else
                        <span>{{ $type }}</span>
                    @endif
                </div>
                <div class="card-body no-padding">
                    <table class="datatable table table-striped primary">
                        <thead>
                        <tr>
                            @foreach($typeEntity::INDEX_FIELD as $field)
                                <th>{{ keyToLabel($field) }}</th>
                            @endforeach
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($list as $item)
                            <tr>
                                @foreach($typeEntity::INDEX_FIELD as $field)
                                    <td @if($item->formType($field) == 'amount') class="autonumeric" @endif>{{ $item->getValue($field, '', '') }}</td>
                                @endforeach
                                <td class="text-center td-button">
                                    @if($typeEntity::ALLOW_MULTIPLE)
                                        <a href="{{ url('/admin/cms/details/'.$type.'/'.get_class_short($item).'/'.$item->getKey()) }}">
                                            <button type="button" class="btn btn-default btn-xs"> Edit Details</button>
                                        </a>
                                    @else
                                        <a href="{{ url('/admin/cms/details/'.$type.'/'.get_class_short($item)) }}">
                                            <button type="button" class="btn btn-default btn-xs"> Edit Details</button>
                                        </a>
                                    @endif
                                    @if($typeEntity::ALLOW_DELETION)
                                        <button type="button" class="btn btn-default btn-xs delete-button-modal" data-toggle="modal" data-target="#modal-delete-alert" data-url="{{ url('/admin/cms/delete/'.$type.'/'.get_class_short($item).'/'.$item->getKey()) }}"> Delete</button>
                                    @endif

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    @include('cms.layouts.part.modal')
@endsection
