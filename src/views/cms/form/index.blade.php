

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header card-header-add">
                    <div>{{ $model::getTitle() }}</div>
                    <a type="button" class="btn btn-success btn-xs pull-right" href=" {{ $model::getUrlDetails().'/0' }}">
                        <i class="icon fa fa-plus"></i>
                        <span class="help-text">Buat {{ $model::getTitle() }} baru</span>
                    </a>
                </div>
                <div class="card-body no-padding">
                    <table class="datatable table table-striped primary">
                        <thead>
                        <tr>
                            @foreach($model::INDEX_FIELD as $field)
                                <th>{{ $model::label($field) }}</th>
                            @endforeach
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($list as $item)
                            <tr>
                                @foreach($model::INDEX_FIELD as $field)
                                    <td>{{ $item->getValue($field) }}</td>
                                @endforeach
                                <td class="text-center td-button">
                                    <a href="{{ $model::getUrlDetails().'/'.$item->id }}">
                                        <button type="button" class="btn btn-default btn-xs"> Details</button>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
