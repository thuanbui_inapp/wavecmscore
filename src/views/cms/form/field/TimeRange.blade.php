<div class="input-group input-date-range" data-date="13/07/2013" data-date-format="mm/dd/yyyy">
	<input id="{{ $model->getFormName($key, $listName, $listIndex, $language) }}From" name="{{ $model->getFormName($key, $listName, $listIndex, $language) }}From" type="text" class="timepickerinput form-control" value="{{ @$model->getValue($key.'From', $listIndex, $language) }}" {{ $model->isDisabled($key) }}>
	<span class="input-group-addon">To</span>
	<input id="{{ $model->getFormName($key, $listName, $listIndex, $language) }}To" name="{{ $model->getFormName($key, $listName, $listIndex, $language) }}To" type="text" class="timepickerinput form-control" value="{{ @$model->getValue($key.'To', $listIndex, $language) }}" {{ $model->isDisabled($key) }}>
</div>