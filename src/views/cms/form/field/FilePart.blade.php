<div>
    @if(!empty(@$file))
        <div class="input-group">
            <input type="text" class="form-control" disabled value="{{ @$file  }}">
            <input type="hidden" class="form-control" name="{{ $model->getFormName($key.$fileKey, $listName, $listIndex, $language) }}" id="{{ $model->getFormName($key.$fileKey, $listName, $listIndex, $language) }}" value="{{ @$file }}">
            <span class="input-group-addon change-file-delete">Delete</span>
        </div>

        <div class="input-group hide">
            <input type="file" class="form-control input-file-upload" accept=".doc, .docx, .txt,.pdf" id="{{ $model->getFormName($key.$fileKey, $listName, $listIndex, $language) }}" label="{{ $model->label($key) }}" {!! $model->getFileLimit($key) !!} data-name="{{ $model->getFormName($key.$fileKey, $listName, $listIndex, $language) }}" data-required="{{ $model->isRequired($key) }}">
        </div>
    @else
        <div class="input-group">
            <input type="file" class="form-control input-file-upload" accept=".doc, .docx, .txt,.pdf" name="{{ $model->getFormName($key.$fileKey, $listName, $listIndex, $language) }}" id="{{ $model->getFormName($key.$fileKey, $listName, $listIndex, $language) }}" {{ $model->isRequired($key) }} label="{{ $model->label($key) }}" {!! $model->getFileLimit($key) !!}>
        </div>
    @endif
</div>