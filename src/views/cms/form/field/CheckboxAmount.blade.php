<div class="input-group checkbox_text">
	<span class="input-group-addon">
		<label class="form-check-label">
			<input type="checkbox" class="form-check-input" {{ $model->isDisabled($key) }}>
		</label>
	</span>
	<span class="input-group-addon">Rp</span>
	<input class="form-control autonumeric" value="{{ $model->getValue($key, $listItem, $language) }}" type="text" required disabled/>
	<input class="autonumericvalue" id="{{ $model->getFormName($key, $listName, $listIndex, $language) }}" name="{{ $model->getFormName($key, $listName, $listIndex, $language) }}" value="{{ $model->getValue($key, $listItem, $language) }}" type="hidden" {{ $model->isDisabled($key) }}/>

</div>

