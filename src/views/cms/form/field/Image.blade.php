<div @if($imageCount == -1) class="image-multiple-wrapper" @endif>
@php
$imageList = $model->getValue($key, $listItem, $language);
@endphp
@if(!empty($imageList))
@foreach($imageList as $imageKey=>$image)
    @include('cms.form.field.ImagePart')
@endforeach
@endif

@if($imageCount == -1)
    @php
    $imageKey = count($imageList);
    $image = null;
    @endphp
    @if(empty($model->isDisabled($key)))
        @include('cms.form.field.ImagePart')
    @endif
@elseif( $imageCount >= count($imageList) )
    @foreach( range(count($imageList), $imageCount)  as $imageKey)
        @include('cms.form.field.ImagePart')
    @endforeach
@endif

</div>