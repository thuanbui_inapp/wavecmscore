<select  class="form-control" id="{{ $model->getFormName($key, $listName, $listIndex, $language) }}" name="{{ $model->getFormName($key, $listName, $listIndex, $language) }}" value="{{ $model->getValue($key, $listItem, $language) }}" {{ $model->isRequired($key) }} {{ $model->isDisabled($key) }} label="{{ $model->label($key) }}">
    @foreach($model->formSelectList($key) as $selectKey=>$selectLabel)
        <option value="{{ $selectKey }}"
                @if($selectKey == $model->getValue($key, $listItem, $language)) selected @endif>{{ $selectLabel }}</option>
    @endforeach
</select>