@if(!empty($model->getValue($key, $listItem, $language)))
    @foreach($model->getValue($key, $listItem, $language) as $fileKey=>$file)
        @include('cms.form.field.FilePart')
    @endforeach
@else
    @foreach( range(count($model->getValue($key, $listItem, $language)), 0)  as $fileKey)
        @include('cms.form.field.FilePart')
    @endforeach
@endif