<select class="fastselect {{ @$fieldClass }}" multiple id="{{ $model->getFormName($key, $listName, $listIndex, $language) }}" name="{{ $model->getFormName($key, $listName, $listIndex, $language) }}" {{ $model->isDisabled($key) }} label="{{ $model->label($key) }}" style="display: none" required>
	@foreach($model->formSelectList($key) as $selectKey=>$selectLabel)
		<option value="{{ $selectKey }}" @if( in_array($selectKey, $model->getValue($key, $listItem, $language))) selected @endif>{{ $selectLabel }}</option>
	@endforeach
</select>

