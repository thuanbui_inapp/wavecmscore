<div class="input-group date timepickerinput">
	<input id="{{ $model->getFormName($key, $listName, $listIndex, $language) }}" name="{{ $model->getFormName($key, $listName, $listIndex, $language) }}" type="text" class=" form-control" value="{{ @$model->getValue($key, $listItem, $language) }}"  {{ $model->isDisabled($key) }}>
	<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
</div>
