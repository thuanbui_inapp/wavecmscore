<div class="input-group date datepickerinput">
	<input id="{{ $model->getFormName($key, $listName, $listIndex, $language) }}" name="{{ $model->getFormName($key, $listName, $listIndex, $language) }}" type="text" class="form-control" value="{{ empty(@$model->getValue($key, $listItem, $language)) ? '' : getDateOnly(@$model->getValue($key, $listItem, $language)) }}"  {{ $model->isDisabled($key) }}>
	<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
</div>
