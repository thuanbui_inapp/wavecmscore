<div class="image-selector-wrapper">
    @if(empty($model->isDisabled($key)))
        <input type="file" class="hidden btn-upload-file" @if(empty(@$imageList[$imageKey])) id="{{ $model->getFormName($key.$imageKey, $listName, $listIndex, $language) }}" name="{{ $model->getFormName($key.$imageKey, $listName, $listIndex, $language) }}" {{ $model->isRequired($key) }} @endif label="{{ $model->label($key) }}" accept=".jpg,.jpeg,.png" {!! $model->getImageLimit($key) !!}/>
    @endif
    @if(!empty(@$imageList[$imageKey]))
    <input type="hidden" id="{{ $model->getFormName($key.$imageKey, $listName, $listIndex, $language) }}" name="{{ $model->getFormName($key.$imageKey, $listName, $listIndex, $language) }}" value="{{ @$imageList[$imageKey] }}"/>
    @endif
    <button class="btn-upload thumbnail" type="button">
        <img @if(!empty(@$imageList[$imageKey])) src="{{ getImageUrl(@$imageList[$imageKey]) }}" @endif/>
        <div @if(!empty(@$imageList[$imageKey])) style="display: none" @endif>
            <i class="fa fa-upload"></i>
            Upload
        </div>
    </button>
    @if(config('cms.displayImageBtn'))
        <i @if(empty(@$imageList[$imageKey])) style="display: none" @endif class="fa fa-search image-display-icon" onclick="setModalDisplayImageAttributes($(this))" data-label="{{ $model->label($key) }}" data-image-src="{{ getImageUrl(@$imageList[$imageKey]) }}" data-filename="{{ @$imageList[$imageKey] }}" data-toggle="modal" data-target="#modal-display-image"></i>
    @endif
    @if(empty($model->isDisabled($key)))
        <i @if(empty(@$imageList[$imageKey])) style="display: none" @endif class="fa fa-trash-o image-delete-icon"></i>
    @endif
</div>

