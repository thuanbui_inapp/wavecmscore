		<input class="form-control {{ @$fieldClass }}" id="{{ $model->getFormName($key, $listName, $listIndex, $language) }}" name="{{ $model->getFormName($key, $listName, $listIndex, $language) }}" {{ $model->isRequired($key) }} {{ $model->isDisabled($key) }} label="{{ $model->label($key) }}" value="" type="password"/>
	</div>
</div>
<div class="col-md-12">
	<label class="col-md-3 control-label">Confirm {{ $model->label($key) }}</label>
	<div class="col-md-9">
		<input class="form-control {{ @$fieldClass }}" id="confirm{{ ucfirst($model->getFormName($key, $listName, $listIndex, $language)) }}" name="confirm{{ $model->getFormName($key, $listName, $listIndex, $language) }}" {{ $model->isRequired($key) }} {{ $model->isDisabled($key) }} label="Confirm {{ $model->label($key) }}" value="" type="password"/>
