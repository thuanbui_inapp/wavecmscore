<div class="form-control summernote {{ @$fieldClass }}" type="text" {{ $model->isRequired($key) }} {{ $model->isDisabled($key) }} label="{{ $model->label($key) }}" style="display:none;">
	{!!  $model->getValue($key, $listItem, $language)  !!}
</div>
<input type="hidden" {{ $model->isDisabled($key) }} id="{{ $model->getFormName($key, $listName, $listIndex, $language) }}" name="{{ $model->getFormName($key, $listName, $listIndex, $language) }}" value="{{ $model->getValue($key, $listItem, $language) }}">

