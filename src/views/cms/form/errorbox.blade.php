<div class="row @if(!$errors->has()) hidden @endif" id="alert-box">
    <div class="col-md-6 col-md-offset-3">
        <div class="alert alert-danger alert-dismissible fade in" role="alert" id="alert-box-details">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            @if($errors->has())
                @foreach ($errors->all() as $error)
                    <strong>Warning!</strong> {{ $error }} <br/>
                @endforeach
            @endif
        </div>
    </div>
</div>