<div class="col-md-12">
    <div class="col-md-9 col-md-offset-3">
        @if(!empty($cancelUrl)) <a class="btn btn-default" href="{{ $cancelUrl }}">{!! getCancelLabel() !!}</a> @endif
        @if( !(count($model::FORM_DISABLED) == 1 && $model::FORM_DISABLED[0] == 'ALL') )<button class="btn btn-primary submitButton" type="button">{!! getSubmitLabel() !!}</button> @endif
    </div>
</div>