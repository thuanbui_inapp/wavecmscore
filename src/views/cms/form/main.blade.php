@if(count(getNonListDetailsSection($model)) > 0 || @$model::USE_META_SET)
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ $model->getTitleDetails() }}</div>
                <div class="card-body">
                    <div class="row">

                        @include('cms.form.section')

                        @if (count($model::FORM_LIST) == 0)
                            @yield('button')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

@foreach($model::FORM_LIST as $listName=>$listType)
    <div class="col-md-12 listSection" name="">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{ keyToLabel($listName) }}
                    <button type="button" class="btn btn-success btn-xs pull-right">
                        <i class="icon fa fa-plus"></i>
                        <span class="help-text add-list">Tambah Row</span>
                    </button>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="sectionContent">
                            @if(!empty($model->getValue($listName, '', $language)))
                                @foreach($model->getValue($listName, '', $language) as $listIndex=>$listItem)
                                    <div class="panel panel-default lobipanelsection" >
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <h4>{{ $listIndex+1 }}</h4>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            @include('cms.form.section')
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>

                        @php
                            $listIndex = -1;
                            $listItem = [];
                        @endphp
                        <div style="display: none" class="sectionTemplate">
                            <div class="panel panel-default lobipanelsection" >
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4></h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    @include('cms.form.section')
                                </div>
                            </div>
                        </div>


                        @if (getLastTypeName($model) == $listName)
                            @yield('button')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach


@if($model::IS_CMS)
    <script>
        var object = {!! json_encode($model) !!};
        var form_list = {!! json_encode($model::FORM_LIST) !!};
        var imagePath = '{{ url('/') . '/assets/upload/sm/' }}';
    </script>
@endif

