<div class="col-md-12">
    <div class="col-md-3">
        <label class="control-label">{{ $model->label($key) }}</label>
        @if(!empty($model->labelHelp($key)))<p class="control-label-help">( {{ $model->labelHelp($key) }} )</p>@endif
    </div>
    <div class="col-md-9">
        @include('cms.form.field.'.$formType)
    </div>
</div>