@php
$isList = true;
if (empty($listType)) {
    $listType = $model::FORM_TYPE;
    $isList = false;
}
@endphp

@if(@$model::USE_META_SET && !$isList)

    <div class="col-md-12">
        <div class="col-md-3">
            <label class="control-label">{{ $model->label('metaTitle', 'META') }}</label>
            @if(!empty($model->labelHelp('metaTitle', 'META')))<p class="control-label-help">( {{ $model->labelHelp('metaTitle', 'META') }} )</p>@endif
        </div>
        <div class="col-md-9">
            <input class="form-control" id="metaTitle" name="{{ $model->getFormName('metaTitle', '', 0, $language) }}" value="{{ $model->getValue('metaTitle', '', $language) }}" type="text" required label="Judul halaman"/>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-3">
            <label class="control-label">{{ $model->label('metaDescription', 'META') }}</label>
            @if(!empty($model->labelHelp('metaDescription', 'META')))<p class="control-label-help">( {{ $model->labelHelp('metaDescription', 'META') }} )</p>@endif
        </div>
        <div class="col-md-9">
            <textarea class="form-control" id="metaDescription" name="{{ $model->getFormName('metaDescription', '', 0, $language) }}" cols="30" rows="5" label="Deskripsi halaman" required>{{ $model->getValue('metaDescription', '', $language) }}</textarea>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-3">
            <label class="control-label">{{ $model->label('metaKeywords', 'META') }}</label>
            @if(!empty($model->labelHelp('metaKeywords', 'META')))<p class="control-label-help">( {{ $model->labelHelp('metaKeywords', 'META') }} )</p>@endif
        </div>
        <div class="col-md-9">
            <input class="form-control" id="metaKeywords" name="{{ $model->getFormName('metaKeywords', '', 0, $language) }}" value="{{ $model->getValue('metaKeywords', '', $language) }}" type="text" required label="Keywords"/>
        </div>
    </div>

@endif

@foreach( $listType as $key=>$value)

    @php
    $formType = $model->formType($key);
    if ($isList) $formType = $listType[$key];

    if( strpos( $formType , 'Image' ) !== false ){
        $imageCount = (substr($formType, 6) - 1);
        $formType = 'Image';
    }

    if (!isset($listItem)) $listItem = '';
    if (!isset($listName)) $listName = '';
    if (!isset($listIndex)) $listIndex = '';
    @endphp

    @if ($formType && $formType != 'ListSortable' && $model->isRemoved($key))
        @include('cms.form.row')
    @endif
@endforeach