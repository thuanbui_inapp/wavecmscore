@extends('cms.layouts.authorized')

@section('headerCustom')
	@php
	    $id = 0;
	    if (!empty($model) && !empty($model->getKey())) $id = $model->getKey();
	
	    if(empty($id)) $title = getPrefixTitleDetails() . ' ' . $model::getTitleDetails();
	    else $title = $model::getTitleDetails();
	
	    $formUrl = $model->getUrlDetails();
	    $cancelUrl = $model->getUrlIndex();
	
	    $language = '';
	
	@endphp
@endsection

@include('cms.form.details')

