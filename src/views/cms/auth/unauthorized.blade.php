@extends('cms.layouts.authorized')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-body">
                    <h3>You're not authorized to access this page.</h3>
                    <a href="{{ url('/admin/login') }}" class="btn btn-primary">Back to Login Page</a>
                </div>
            </div>
        </div>
    </div>
@endsection