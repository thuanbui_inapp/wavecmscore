<?php

/*
 * This file is part of jwt-auth.
 *
 * (c) Sean Tymon <tymon148@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WVI\CMSCore\Facades;

use App\Service\CMSCore\ComponentService;
use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\Route;

class CMSCore extends Facade
{
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor(){
		return 'wvi.cmscore';
	}

	public static function CRUDRoute($single, $plural = '', $routeEnable = [], $checkPermission = '', $moduleName = '') {
		if (empty($plural)) $plural = $single.'s';
		if (count($routeEnable) != 5) $routeEnable = [true, true, true, true, true];
		if ($checkPermission === '' && config('cms.acl') === true) $checkPermission = true; 
		if (empty($moduleName)) $moduleName = strtoupper($single); 
		$controller = 'Admin\\'.ucfirst($single).'Controller';

		if($checkPermission) {
			if($routeEnable[0]) Route::get('/'.$plural, ['uses' => $controller.'@index', 'middleware' => 'acl', 'permission' =>  [$moduleName.'_READ']])->name('admin.'.$plural);
			if($routeEnable[1]) Route::get('/'.$single.'/{id?}', ['uses' => $controller.'@details', 'middleware' => 'acl', 'permission' =>  [$moduleName.'_READ']])->name('admin.'.$single);
			if($routeEnable[2]) Route::post('/'.$single.'/{id?}', ['uses' => $controller.'@save', 'middleware' => 'acl', 'permission' =>  [$moduleName.'_CREATE',$moduleName.'_UPDATE']])->name('admin.'.$single.'.save');
			if($routeEnable[3]) Route::get('/'.$single.'/delete/{id?}', ['uses' => $controller.'@delete', 'middleware' => 'acl', 'permission' =>  [$moduleName.'_DELETE']])->name('admin.'.$single.'.delete');
			if($routeEnable[4]) Route::post('/'.$plural, ['uses' => $controller.'@indexData', 'middleware' => 'acl', 'permission' =>  [$moduleName.'_READ']])->name('admin.'.$plural.'Data');
		} else {
			if($routeEnable[0]) Route::get('/'.$plural, $controller.'@index')->name('admin.'.$plural);
			if($routeEnable[1]) Route::get('/'.$single.'/{id?}', $controller.'@details')->name('admin.'.$single);
			if($routeEnable[2]) Route::post('/'.$single.'/{id?}', $controller.'@save')->name('admin.'.$single.'.save');
			if($routeEnable[3]) Route::get('/'.$single.'/delete/{id?}', $controller.'@delete')->name('admin.'.$single.'.delete');
			if($routeEnable[4]) Route::post('/'.$plural, $controller.'@indexData')->name('admin.'.$plural.'Data');
		}
	}

	public static function AutoComplete($class, $path = '', $controller = '', $routeName = '') {
		$className = explode('\\', $class);
		$className = $className[count($className) - 1];
		$className = strtolower($className);

		if (empty($path)) $path = '/' . $className . '/search';
		if (empty($routeName)) $routeName = 'admin.' . $className . '.autocomplete';

		if (empty($controller)){
			Route::any($path, function() use($class){ return ComponentService::AutoCompleteSearch($class); })->name($routeName);
		} else {
			Route::any($path, $controller)->name($routeName);
		}
	}

	public static function GaRoute() {
		Route::any('ga/ajax/sessions', 'CMSCore\Admin\GoogleAnalyticController@ajaxSessions')->name('admin.ga.ajaxSessions');
		Route::any('ga/ajax/users', 'CMSCore\Admin\GoogleAnalyticController@ajaxUsers')->name('admin.ga.ajaxUsers');
	}

	public static function Notification() {
		Route::get('notification/my/list',  'CMSCore\Admin\NotificationController@index')->name('admin.notification.my.list');
		Route::get('notification/my/badge/{type}',  'CMSCore\Admin\NotificationController@badge')->name('admin.notification.my.badge');
		Route::get('notification/my/readall',  'CMSCore\Admin\NotificationController@readAll')->name('admin.notification.my.readall');
		Route::get('notification/my/topic/{notificationId}',  'CMSCore\Admin\NotificationController@topic')->name('admin.notification.my.topic');
	}

	public static function Auth(){
	    Route::get('auth/socialite/redirect/{provider}', 'CMSCore\Admin\AuthController@socialRedirect')->name('admin.auth.socialiteRedirect');
        Route::get('auth/socialite/handle/{provider}', 'CMSCore\Admin\AuthController@socialHandle')->name('admin.auth.socialiteHandle');
    }

    public static function Unauthorized(){
        Route::get('auth/nopermission', 'CMSCore\Admin\AuthController@noPermission')->name('admin.auth.nopermission');
        Route::get('auth/unauthorized', 'CMSCore\Admin\AuthController@unauthorized')->name('admin.auth.unauthorized');
    }
}
