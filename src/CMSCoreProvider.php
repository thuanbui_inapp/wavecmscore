<?php

namespace WVI\CMSCore;

use App\Blade\CustomBlade;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;

class CMSCoreProvider extends ServiceProvider {
	public function boot() {
		ini_set('date.timezone', env('APP_TIMEZONE', 'Asia/Jakarta'));
		self::IncludeMultipleFile([
			'/app/Blade/',
			'/app/CMSTrait/',
			'/app/Entity/Base/',
			'/app/Exceptions/',
			'/app/Http/Controllers/',
			'/app/Http/Controllers/Admin/',
			'/app/Http/Controllers/Auth/',
			'/app/Http/Middleware/',
			'/app/Helpers/',
			'/app/Jobs/',
			'/app/Queue/',
			'/app/Scopes/',
			'/app/Service/',
			'/app/Util/',
			'/app/Util/DataTable/',
		]);
		Config::set('view.paths', array_merge(Config::get('view.paths'), [
			__DIR__.'/views',
		]));

		CustomBlade::LoadCustomBlade();
	}

	public static function IncludeMultipleFile($pathList) {
		foreach($pathList as $path) {
			foreach(glob(__DIR__.$path.'*.php') as $filePhp) {
				require_once($filePhp);
			}
		}
	}

	public function register() {
		// TODO: Implement register() method.
	}

}