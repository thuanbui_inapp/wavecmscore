var vueMethods = {
	formSubmit(event) {
		try {
			var toSubmit = true;
			if (typeof formSubmit !== 'undefined'){
				this.$data.errors.splice(0, this.$data.errors.length);
				var toSubmit = formSubmit(this.$data);
				if (toSubmit == undefined) {
					toSubmit = (this.$data.errors.length == 0) ? true : false;
				}
			}
			if (toSubmit){
				$('.vueSubmit').attr('data-style', 'zoom-in');
	            $('.vueSubmit').each(function(){
	                var vueBtn = $(this).ladda();
	                vueBtn.ladda('start');
	            });
				$('#mainForm').addHiddenInputData(this.$data);
			} else {
				event.preventDefault();
				$('html, body').animate({scrollTop: '0px'}, 300);
			}
		} catch(e) {
			console.error(e);
			event.preventDefault();
		}
	}
};

var vueWatch = {};

if (typeof vueExtraMethods !== 'undefined') vueMethods = $.extend({}, vueMethods, vueExtraMethods);
if (typeof vueExtraWatch !== 'undefined') vueWatch = $.extend({}, vueWatch, vueExtraWatch);

var vueData = (typeof vueData !== 'undefined') ? $.extend({}, vueData, {errors:[]}) : {errors:[]};
$('[v-model]').each(function(vmodel){
	var name = $(this).attr('v-model');
	if(name.indexOf('.') === -1 && vueData[name] == undefined) {
		vueData[name] = '';
	}
})

var vueOptions = {
	el: '#vueApp',
	data: (typeof vueData !== 'undefined') ? $.extend({}, vueData, {errors:[]}) : {errors:[]},
	methods: vueMethods,
	watch : vueWatch
};
if (typeof vueExtraOptions !== 'undefined') vueOptions = $.extend({}, vueOptions, vueExtraOptions);

$('input[type=file]').each(function(){
	$(this).attr('name', $(this).attr('v-model'))
});
var vueApp = new Vue(vueOptions);

