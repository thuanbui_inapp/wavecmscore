CREATE TABLE IF NOT EXISTS `user` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`email` VARCHAR(50) NULL DEFAULT NULL,
	`password` VARCHAR(100) NULL DEFAULT NULL,
	`name` VARCHAR(100) NULL DEFAULT NULL,
	`rememberToken` VARCHAR(100) NULL DEFAULT NULL,
	`createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`createdBy` BIGINT(20) NULL DEFAULT NULL,
	`updatedAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updatedBy` BIGINT(20) NULL DEFAULT NULL,
	`deletedAt` TIMESTAMP NULL DEFAULT NULL,
	`deletedBy` BIGINT(20) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
);