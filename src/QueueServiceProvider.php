<?php
namespace WVI\CMSCore;

use App\Queue\DatabaseConnector; 
use Illuminate\Queue\QueueServiceProvider as DefaultQueueServiceProvider; 

class QueueServiceProvider extends DefaultQueueServiceProvider {
    protected function registerDatabaseConnector($manager)
    {
        $manager->addConnector('database', function () {
            return new DatabaseConnector($this->app['db']);
        });
    }
}
