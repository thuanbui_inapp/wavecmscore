<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

abstract class Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, Queueable;

    /*
    handle function should be defined as follow in child classes
    public function handle() {
		try {
	
		} catch(\Exception $e) {
			$this->failed($e);
		}
    }
    */

    public function failed(\Exception $e = null) {

    }
}
