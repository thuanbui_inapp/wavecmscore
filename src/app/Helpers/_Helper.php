<?php

use App\Service\CMSCore\PermissionService; 
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;

function checkPermission($permissions) {
	if(config('cms.acl') === true) {
		if(empty($permissions)) $permissions = []; 
        if(is_string($permissions)) $permissions = [$permissions]; 

        $userPermissions = PermissionService::GetUserPermission(); 
        if(empty(array_intersect($permissions, $userPermissions))) return false; 
    }
    return true; 
}

function checkbox($key, $defaultValue, $checkBoxKey){
	$value = old($key,  $defaultValue);

	if($value == $checkBoxKey) return 'checked selected';
	return '';
}

function getDateOnly($stringDate){
	return (new Carbon($stringDate))->format('d M Y');
}

function getMonthYearOnly($stringDate){
	return (new Carbon($stringDate))->format('M\`y');
}

function getTimeOnly($stringDate){
	return (new Carbon($stringDate))->format('h:i:s A');
}

function getDays($stringDate){
	return Carbon::now()->diffInDays(new Carbon($stringDate));
}

function getValueFromMap($key, $map){
	$value = "";
	if (isset($map[$key])) $value = $map[$key];

	return $value;
}

function labelStatus($key){
	$map = [
		"ACTIVE" => "Active",
		"COMPLETED" => "Completed",
		"CANCELLED" => "Cancelled",
		"PENDING_PAYMENT" => "Pending Payment",
	];
	return getValueFromMap($key, $map);
}

function labelRole($key){
	$map = [
		"ORGANIZER" => "Nazhir Admin",
		"ADMIN" => "Admin",
		"DONOR" => "Wakif",
	];
	return getValueFromMap(strtoupper($key), $map);
}

function labelProjectCategory($key){
	return getValueFromMap($key, getProjectCategoryMap());
}

function labelEnable($key){
	$map = [
		"Y" => "Enabled",
		"N" => "Disabled",
	];
	return getValueFromMap($key, $map);
}

function getImageUrl($filename){
	if (empty($filename)) return getNoPhoto();
	return url('/') . '/assets/upload/md/' . $filename;
}

function getImageUrlSize($filename, $size){
	if (empty($filename)) return getNoPhoto();
	return url('/') . '/assets/upload/' . $size .'/'. $filename;
}

function getNoPhoto(){
	return url('/') . '/assets/admin/image/no-photo.png';
}


function randomKey($length) {
	$pool = array_merge(range(0,9), range('a', 'z'),range('A', 'Z'));

	$key = '';
	for($i=0; $i < $length; $i++) {
		$key .= $pool[mt_rand(0, count($pool) - 1)];
	}
	return $key;
}

function keyToLabel($key){
	$key = Str::snake($key);
	return ucfirst(strtolower(str_replace("_", " ", $key)));
}

function nameToEntity($name){
	return 'App\\Entity\\CMS\\'.$name;
}

function nameToBaseEntity($name){
	return 'App\\Entity\\Base\\'.$name;
}

function get_class_short($class) {
	$path = explode('\\', get_class($class));
	return array_pop($path);
}

function listToTokenfield($list, $id, $value){
	$tokenFields = [];
	foreach($list as $item){
		$tokenFields[] = ['id' => $item->$id, 'value' => $item->$value];
	}
	return $tokenFields;
}

function arrayToTokenfield($list){
	$tokenFields = [];
	foreach($list as $item){
		$tokenFields[] = ['id' => $item, 'value' => keyToLabel($item)];
	}
	return $tokenFields;
}

function toCurrency($number){
	return number_format($number,0,",",".");
}

function labelInvoiceStatus($key){
	$map = [
		Invoice::STATUS_NEW => "Belum Cair",
		Invoice::STATUS_DISBURSED => "Tercairkan",
		Invoice::STATUS_COMPLETED => "Selesai",
	];
	return getValueFromMap($key, $map);
}

function getLang () {
	if (empty(config('selectedLanguage'))) {
		return 'en';
	}
	return config('selectedLanguage');
}

function appendUrl(){
	if (config('selectedLanguage') != 'en') return '?lang='.config('selectedLanguage');
	return '';
}

function isActiveRoute($arrayRouteName){
	foreach($arrayRouteName as $routeName){
		if(Request::url() == route($routeName)){
			return ' active ';
		}
	}
	return '';
}

function inputObject($input = []) {
	return arrayToObject(Input::all()); 
} 

function arrayToObject($input) {
	if(is_array($input)) {
		foreach($input as &$data) $data = arrayToObject($data); 
		if(isAssociativeArray($input)) $input = (object)$input; 
	} 
	return $input; 
}

function isSequentialArray(array $arr) {
	if(isAssociativeArray($arr)) return false; 
	return true; 
}

function isAssociativeArray(array $arr){
	if(array() === $arr) return false;
    return array_keys($arr) !== range(0, count($arr) - 1);
}

function encodeImage($imageFile, $imageExtension) {
    return 'data:image/' . $imageExtension . ';base64,' . base64_encode(file_get_contents($imageFile)); 
}

function cleanData($data) {
	if(is_string($data)) return removeBadCharacters($data);
	if(is_object($data)) $data = (array)$data; 

	foreach($data as &$attribute) {
		if(is_string($attribute) && strpos($attribute, 'data:image') === false) $attribute = removeBadCharacters($attribute);
		if(is_array($attribute) || is_object($attribute)) $attribute = cleanData($attribute);
	}
	if(isAssociativeArray($data)) $data = (object) $data;
	return $data;
}

function removeBadCharacters($string, $replacement = '') {
	return str_replace(['&', '<', '>', '/', '\\', '"', "'", '?', '+'], $replacement, $string);
}

function getPermalink($name, $id) {
    if(empty($name) || empty($id)) return '';
    return preg_replace("![^a-z0-9]+!i", "-", $name).'-'.$id;
}

function parsePermalinkToId($permalink) {
    $permalink = explode('-', $permalink);
    return end($permalink);
}

function getParentLang() {
    if (empty(Config::get('cms.LANGUANGE_PARENT'))) {
        \App::setLocale('id');
        return 'id';
    }

    \App::setLocale(Config::get('cms.LANGUANGE_PARENT'));
    return Config::get('cms.LANGUANGE_PARENT');
}

function getSelectedLang () {
    if (empty(config('selectedLanguage'))) {
        \App::setLocale(Config::get('cms.LANGUANGE_PARENT'));

        return Config::get('cms.LANGUANGE_PARENT');
    }

    \App::setLocale(config('selectedLanguage'));
    return config('selectedLanguage');
}

function langAppendUrl(){
    if (config('selectedLanguage') != Config::get('cms.LANGUANGE_PARENT')) return '?lang='.config('selectedLanguage');

    return '';
}

function langAppendUrlPagination(){
    if (config('selectedLanguage') != Config::get('cms.LANGUANGE_PARENT')) return '&lang='.config('selectedLanguage');

    return '';
}

function elapsedTime($dateString) {
	if ($dateString) {
		$time = Carbon::parse($dateString);
		$timeNow = Carbon::now();
		$difference = $time->diff($timeNow);

		$seconds = $time->diffInSeconds($timeNow);
		$minutes = $time->diffInMinutes($timeNow);
		$hours = $time->diffInHours($timeNow);
		$days = $time->diffInDays($timeNow);

		if ($days > 0) {
			return $dateString;
		} elseif($hours > 0) {
			return $hours . ' hour(s) ago';
		} elseif($minutes >0 ) {
			return $minutes . ' minute(s) ago';
		} else {
			return "few second(s) ago";
		}
	}
	return $dateString;
}

function getDataTableHeader($object){
    $tableHeader = [];
    if (is_array($object)) {
        $tableHeader = $object;
    } else if (class_exists($object) && is_array($object::INDEX_FIELD)){
        foreach($object::INDEX_FIELD as $key){
            $tableHeader[$key] = $object::label($key);
        }
    }
    return $tableHeader;
}

function getDataTableList($rawList, $headers){
	if (!is_array($rawList)) return [];

	$list = [];
	foreach($rawList as $rawItem){
		$item = [];
		$rawItemArray = (array) $rawItem;
		foreach($headers as $headerKey => $headerLabel){
            $item[] = getListValue($rawItemArray,$headerKey);
		}
		$list[] = $item;
	}
	return $list;
}

function generatePastDays(){
    $days = [7,14,30];

    $arguments = func_get_args();

    if (count($arguments) > 0) $days = $arguments;

    return $days;
}

function getLastNdaysDates($numberOfDays = 7, $format = 'M d, Y'){
    $dates = [];
    $now = \Carbon::now()->subDays($numberOfDays);

    for ($i = 0; $i <= ($numberOfDays); $i++){
        $dates[] = $now->copy()->addDays($i)->format($format);
    }

    return $dates;
}

function getListValue($instance, $string){
    if (strpos($string, '.') !== false) {
        $params = explode('.', $string);
        $obj = $instance[$params[0]];
        foreach ($params as $key => $param){
            if ($key === 0) continue;
            $obj = isset($obj->{$param}) ? $obj->{$param} : '';
        }

        return $obj;
    }
    $returnValue = isset($instance[$string]) ? $instance[$string] : '';
    return $returnValue;
}
