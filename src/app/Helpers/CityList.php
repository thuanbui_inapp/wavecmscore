<?php

function getListProvince() {
	$map = [
		"Aceh" => [
			"Banda Aceh",
			"Langsa",
			"Lhokseumaw",
			"Meulaboh",
			"Sabang",
			"Subulussalam",
		],
		"Bali" => [
			"Denpasar",
		],
		"Bangka Belitung" => [
			"Pangkalpinang",
		],
		"Banten" => [
			"Cilegon",
			"Serang",
			"Tangerang Selatan",
			"Tangerang",
		],
		"Bengkulu" => [
			"Bengkulu",
		],
		"Gorontalo" => [
			"Gorontalo",
		],
		"Jakarta" => [
			"Jakarta Barat",
			"Jakarta Pusat",
			"Jakarta Selatan",
			"Jakarta Timur",
			"Jakarta Utara",
		],
		"Jambi" => [
			"Sungai Penuh",
			"Jambi",
		],
		"Jawa Barat" => [
			"Bandung",
			"Bekasi",
			"Bogor",
			"Cimahi",
			"Cirebon",
			"Depok",
			"Sukabumi",
			"Tasikmalaya",
			"Banjar",
		],
		"Jawa Tengah" => [
			"Magelang",
			"Pekalongan",
			"Purwokerto",
			"Salatiga",
			"Semarang",
			"Surakarta",
			"Tegal",
		],
		"Jawa Timur" => [
			"Batu",
			"Blitar",
			"Kediri",
			"Madium",
			"Malang",
			"Mojokerto",
			"Pasuruan",
			"Probolinggo",
			"Surabaya",
		],
		"Kalimantan Barat" => [
			"Pontianak",
			"Singkawang",
		],
		"Kalimantan Selatan" => [
			"Banjarbaru",
			"Banjarmasin",
		],
		"Kalimantan Tengah" => [
			"Palangkaraya",
		],
		"Kalimantan Timur" => [
			"Balikpapan",
			"Bontang",
			"Samarinda",
		],
		"Kalimantan Utara" => [
			"Tarakan",
		],
		"Kepulauan Riau" => [
			"Batam",
			"Tanjungpinang",
		],
		"Lampung" => [
			"Bandar Lampung",
			"Kotabumi",
			"Liwa",
			"Metro",
		],
		"Maluku Utara" => [
			"Ternate",
			"Tidore Kepulauan",
		],
		"Maluku" => [
			"Ambon",
			"Tual",
		],
		"Nusa Tenggara Barat" => [
			"Bima",
			"Mataram",
		],
		"Nusa Tenggara Timur" => [
			"Kupang",
		],
		"Papua Barat" => [
			"Sorong",
		],
		"Papua" => [
			"Jayapura",
		],
		"Riau" => [
			"Dumai",
			"Pekanbaru",
		],
		"Sulawesi Selatan" => [
			"Makassar",
			"Palopo",
			"Parepare",
		],
		"Sulawesi Tengah" => [
			"Palu",
		],
		"Sulawesi Tenggara" => [
			"Bau-Bau",
			"Kendari",
		],
		"Sulawesi Utara" => [
			"Bitung",
			"Kotamobagu",
			"Manado",
			"Tomohon",
		],
		"Sumatera Barat" => [
			"Bukittinggi",
			"Padang",
			"Padangpanjang",
			"Pariaman",
			"Payakumbuh",
			"Sawahlunto",
			"Solok",
		],
		"Sumatera Selatan" => [
			"Lubuklinggau",
			"Pagaralam",
			"Palembang",
			"Prabumulih",
		],
		"Sumatera Utara" => [
			"Binjai",
			"Medan",
			"Padang Sidempuan",
			"Pematangsiantar",
			"Sibolga",
			"Tanjungbalai",
			"Tebingtinggi",
		],
		"Yogyakarta" => [
			"Yogyakarta",
		],
	];

	return $map;
}

function getListCity(){
	$map = [];
	foreach(getListProvince() as $cities){
		array_merge($map, $cities);
	}
	return $map;
}