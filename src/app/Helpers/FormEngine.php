<?php

function getFormRequiredArray($model){
    $formRequiredArray = [];

    $source = $model::FORM_REQUIRED;
    if (count($source) == 1 && $source[0] == 'ALL'){
        foreach($model::FORM_TYPE as $key=>$type){
            if ($type == 'date_range'){
                $formRequiredArray[] = $key.'_from';
                $formRequiredArray[] = $key.'_to';
            }
            else
                $formRequiredArray[] = $key;
        }
    } else {
        foreach($source as $key){
            $formRequiredArray[] = $key;
        }
    }

    return $formRequiredArray;
}
function getFormImageArray($model){
    $formImageArray = [];
    foreach($model::FORM_TYPE as $key=>$type){
        if (substr($type,0,5) == 'Image') $formImageArray[] = $key;
    }
    return $formImageArray;
}
function getNonListDetailsSection($model){
    $nonListType = [];
    $list = $model::FORM_LIST;
    foreach($model::FORM_TYPE as $key=>$type){
        if (!isset($list[$key])){
            $nonListType[] = $key;
        }
    }
    return $nonListType;
}
function getLastTypeName($model){
    $name = '';
    foreach($model::FORM_TYPE as $key=>$type){
        $name = $key;
    }
    return $name;
}

function GetDataValue($data, $key, $language){
    if (!isset($data[$key])) return '';

    if (empty($language)) {
        return $data[$key];
    }
    else {
        if (!isset($data[$key][$language])) return '';
        return $data[$key][$language];
    }
}
function languangeLabel(){
	$map = [
		'en' => 'English',
		'id' => 'Indonesia',
		'zh_SG' => 'Simplified Chinese',
	];
	return $map;
}