<?php

namespace App\Blade;

class CustomBlade {
	public static function LoadCustomBlade() {
		Util::ifyes();
		Digital::google();
		Assets::js();
		Assets::css();
		Feature::notification();
		DataTable::tableajax();
		DataTable::endtableajax();
		DataTable::tableth();
		DataTable::tableconfig();
		GoogleAnalytic::sessions();
		GoogleAnalytic::users();
		GoogleAnalytic::pagevisits();
        GoogleAnalytic::acquisition();
	}

}