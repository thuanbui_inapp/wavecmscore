<?php

namespace App\Blade;

class Feature {
	public static function notification(){
		\Blade::directive('notificationbadge', function($expression) {
			return "<?php echo \$__env->make( 'cms.feature.notification.badge' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>";
		});
	}
}