<?php

namespace App\Blade;

class GoogleAnalytic {
	public static function sessions(){
		\Blade::directive('gasessions', function($expression) {
			return "<?php echo \$__env->make( 'cms.feature.ga.sessions' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>";
		});
	}
	public static function users(){
		\Blade::directive('gausers', function($expression) {
			return "<?php echo \$__env->make( 'cms.feature.ga.users' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>";
		});
	}
	public static function pagevisits(){
		\Blade::directive('gapagevisits', function($expression) {
			return "<?php echo \$__env->make( 'cms.feature.ga.pagevisits' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>";
		});
	}
	public static function acquisition(){
		\Blade::directive('gaacquisition', function($expression) {
			return "<?php echo \$__env->make( 'cms.feature.ga.acquisition' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>";
		});
	}
}


