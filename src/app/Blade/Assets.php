<?php

namespace App\Blade;

class Assets {
	public static function js(){
		\Blade::directive('js', function($expression) {
			return "<?php if(\File::exists(public_path$expression)) { ?><script src=\"{{ url$expression }}?v={{ md5_file(public_path($expression)) }}\"></script><?php } ?>";
		});
	}
	public static function css(){
		\Blade::directive('css', function($expression) {
			return "<?php if(\File::exists(public_path$expression)) { ?><link href=\"{{ url$expression }}?v={{ md5_file(public_path($expression)) }}\" rel=\"stylesheet\"></link><?php } ?>";
		});
	}
}