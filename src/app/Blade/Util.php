<?php

namespace App\Blade;

class Util {
	public static function ifyes(){
		\Blade::directive('ifyes', function($expression) {
			return "<?php if(isset$expression && $expression == 'Y'): ?>";
		});
	}
}