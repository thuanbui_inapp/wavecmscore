<?php

namespace App\Blade;

class Digital {
	public static function google(){
		\Blade::directive('googleAnalytics', function($expression) {
			if ($expression == '()' && !empty(env('GOOGLE_ANALYTICS_TRACKING_ID')) ) $expression = "(env('GOOGLE_ANALYTICS_TRACKING_ID'))";
			return \Blade::compileString("@include('cms.digital.googleAnalytics', ['trackingId' => ".$expression." ])");
		});
		\Blade::directive('googleTagManagerHead', function($expression) {
			if ($expression == '()' && !empty(env('GOOGLE_TAG_MANAGER_CONTAINER_ID')) ) $expression = "(env('GOOGLE_TAG_MANAGER_CONTAINER_ID'))";
			return \Blade::compileString("@include('cms.digital.googleTagManagerHead', ['containerId' => ".$expression." ])");
		});
		\Blade::directive('googleTagManagerBody', function($expression) {
			if ($expression == '()' && !empty(env('GOOGLE_TAG_MANAGER_CONTAINER_ID')) ) $expression = "(env('GOOGLE_TAG_MANAGER_CONTAINER_ID'))";
			return \Blade::compileString("@include('cms.digital.googleTagManagerBody', ['containerId' => ".$expression." ])");
		});
	}
}