<?php

namespace App\Entity\Base;

use App\Util\CMSCore\CodingConstant;

class User extends BaseEntity {
	const ROLE_ADMIN = "ADMIN";

	const STATUS_ACTIVE = "ACTIVE";

	protected $table = 'user';
	protected $fillable = ['name', 'email', 'phone', 'status'];
	protected $hidden = ['password', 'rememberToken', 'deletedAt', 'deletedBy'];

	const TITLE_DETAILS = "User Details";

	const ROUTE_DETAILS = 'admin.user';
	const ROUTE_INDEX = 'admin.users';

	const FORM_REQUIRED = ['name', 'email', 'roles'];
	const INDEX_FIELD = [
		'name','email', 'phone'
	];
	const FORM_TYPE = [
		'name' => 'Text',
		'email'  => 'Text',
		'phone'  => 'Number',
		'password'  => 'Password',
		'roles'  => 'FastSelect',
	];
	const FORM_DISABLED = ['email'];

	const FORM_SELECT_LIST = [
		'roles' => 'GetRoles'
	];
	const MANUAL_SAVE_FIELD = [
		'roles'
	];
	const FORM_LABEL = [
		'password' => 'New Password'
	];

	// @Override Authenticable function which causes error
	public function getRememberTokenName()
	{
		return CodingConstant::ConvertCase('remember_token');
	}
	public function userPermissions() {
		return $this->hasMany(Permission::class, 'userId');
	}
	public function roles(){
		return $this->hasMany(Role::class, 'userId');
	}
	public function isRole($roleName){
		foreach($this->roles as $role){
			if ($roleName == $role->name) return true;
		}
		return false;
	}

}
