<?php

namespace App\Entity\Base;

class Object extends CMS {
    const CMS_TYPE = 'Object';
    const FORM_REQUIRED = [];
    const ALLOW_MULTIPLE = true;

    static function getObjectForList(){
        $subtype = static::getClassName();

        return CMS::where('type', 'Object')
            ->where('subtype', $subtype)
            ->get();
    }

    static function  getObjectByID($id){
        $subtype = static::getClassName();

        return CMS::where('id', $id)
            ->where('subtype', $subtype)
            ->get()
            ->first();
    }

    const INDEX_FIELD = [];
}
