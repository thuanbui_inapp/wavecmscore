<?php

namespace App\Entity\Base;

class Role extends BaseEntity {

    protected $table = 'user_role';
    protected $fillable = ['name', 'user_id'];
}
