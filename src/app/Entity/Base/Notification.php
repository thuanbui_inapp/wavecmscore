<?php

namespace App\Entity\Base;

class Notification extends BaseEntity {
	protected $table = 'notification';
	protected $fillable = ['userId', 'topicId', 'topicType', 'topicTable', 'status', 'url', 'title', 'message', 'notificationDate'];
	protected $hidden = ['deletedAt', 'deletedBy'];

	public function topic(){
		return $this->morphTo();
	}
	public function user(){
		return $this->belongsTo(User::class);
	}
}
