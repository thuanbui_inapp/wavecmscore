<?php

namespace App\Entity\Base;

class Permission extends BaseEntity {

    protected $table = 'user_permission';
    protected $fillable = ['name', 'user_id'];
}
