<?php

namespace App\Entity\Base;

use App\CMSTrait\SingleImageTrait;
use App\Entity\Attribute\AttributeBase;
use App\Entity\Base\BaseEntity;
use Illuminate\Support\Facades\Config;


class LangEntity extends BaseEntity {

    private $parentLang;

    public function __construct(){
        parent::__construct();
        $this->parentLang = Config::get('cms.LANGUANGE_PARENT');
    }

    public function getChildModelByLang($language) {
        $class = get_class($this);
        $childModel = $class::where('parentId', $this->id)->where('lang', $language)->get()->first();

        if (!$childModel) {
            $childModel = new $class();
        }

        return $childModel;
    }

    public function getValue($key, $listItem, $language){
        if ($language == '') {
            $language = $this->parentLang;
        }

        if (empty($listItem)) {

            if ($language == $this->parentLang) {
                $model = $this;
            }else {
                $model = $this->getChildModelByLang($language);
            }

            if (!static::IS_CMS && (substr(static::formType($key),0,5) == 'Image' || substr(static::formType($key),0,4) == 'File') && !is_array(@$model->$key) ){
                if (substr(@$model->$key,0,1) == '[')
                    return json_decode(@$model->$key);
                else
                    return [@$model->$key];
            }

            if (static::formType($key) == 'FastSelect'){
                return @$model->$key->pluck('id')->toArray();
            }
            return @$model->$key;
        }
        else return @$listItem->$key;
    }

    public function getSelectedLangDataAttribute() {
        $selectedLang = getSelectedLang();

        $class = get_class($this);

        $selectedLangData = new $class();

        if ($this->lang != $selectedLang){
            $selectedLangData = $class::where('parentId', $this->id)->where('lang', $selectedLang)->get()->first();

            if (!$selectedLangData){
                $selectedLangData = $this;
            }
        }else {
            $selectedLangData = $this;
        }

        return $selectedLangData;
    }

    public function isChild() {
        if ($this->parentId){
            return true;
        }

        return false;
    }

    public function isParent() {
        if ($this->parentId){
            return false;
        }

        return true;
    }





}
