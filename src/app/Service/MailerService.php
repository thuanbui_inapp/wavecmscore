<?php
namespace App\Service\CMSCore;

use Illuminate\Support\Facades\Mail;

class MailerService {


    public static function invite($user, $password){
        $details = [
            'name' => $user->name,
            'inviterName' => \Auth::user()->name,
            'password' => $password
        ];

        $emailTo = $user->email;

        try {
            Mail::send('email.invite', $details,
                function ($message) use ($emailTo) {
                    $message
                        ->to($emailTo)
                        ->from('wakafhasanah@bnisyariah.co.id')
                        ->subject('[BNI - Wakaf] Come and join us');
                });
            \Log::info('NEW USER - ' . $emailTo . ' - ' . $password);
        }catch(\Exception $msg){
            \Log::error($msg);
        }

    }

    public static function reset($user, $password){
        $details = [
            'name' => $user->name,
            'email' => $user->email,
            'password' => $password
        ];

        $emailTo = $user->email;

        try {
            Mail::send('email.reset', $details,
                function ($message) use ($emailTo) {
                    $message
                        ->to($emailTo)
                        ->from('wakafhasanah@bnisyariah.co.id')
                        ->subject('[BNI - Wakaf] Password Reset');
                });
            \Log::info('RESET USER - ' . $emailTo . ' - ' . $password);
        }catch(\Exception $msg){
            \Log::error($msg);
        }

    }

    public static function register($data){
        $details = [
            'name' => $data['name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'password' => $data['password']
        ];

        $emailTo = $data['email'];

        try {
            Mail::send('email.register', $details,
                function ($message) use ($emailTo) {
                    $message
                        ->to($emailTo)
                        ->from('wakafhasanah@bnisyariah.co.id')
                        ->subject('[BNI - Wakaf] Registration');
                });
        }catch(\Exception $msg){
            \Log::error($msg);
        }
    }
}
