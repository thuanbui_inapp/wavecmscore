<?php
namespace App\Service;



use App\Entity\City;
use App\Entity\Province;
use App\Entity\Subdistrict;
use App\Entity\Village;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class GoogleAnalyticCoreService {
	public static function GetData($startDate, $endDate, $type, $options) {
		$analytics = static::init();
		$profileId = static::GetFirstProfileId($analytics);

		$cachedData = static::GetCachedData($analytics, $profileId, $startDate, $endDate, $type, $options);
		return $cachedData;
	}

	private static function Init() {
		// Creates and returns the Analytics Reporting service object.

		// Use the developers console and download your service account
		// credentials in JSON format. Place them in this directory or
		// change the key file location if necessary.

		// Create and configure a new client object.
		$client = new \Google_Client();
		$client->setApplicationName("Analytics Reporting");
		$client->setAuthConfig(config_path('service-account-credentials.json'));
		$client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
		$analytics = new \Google_Service_Analytics($client);

		return $analytics;
	}

	private static function Test($analytics) {
		try {
			$accounts = $analytics->management_accountSummaries->listManagementAccountSummaries();
		} catch (apiServiceException $e) {
			\Log::error('There was an Analytics API service error ' . $e->getCode() . ':' . $e->getMessage());

		} catch (apiException $e) {
			Log::errro('There was a general API error ' . $e->getCode() . ':' . $e->getMessage());
		}
	}

	private static function GetFirstProfileId($analytics) {
		// Get the user's first view (profile) ID.
        if (\Cache::has('firstProfileId')) {
            $firstProfileId = \Cache::get('firstProfileId');

            if (!empty($firstProfileId)) return $firstProfileId;
        }
		// Get the list of accounts for the authorized user.
		$accounts = $analytics->management_accounts->listManagementAccounts();

		if (count($accounts->getItems()) > 0) {
			$items = $accounts->getItems();
			$firstAccountId = $items[0]->getId();

			// Get the list of properties for the authorized user.
			$properties = $analytics->management_webproperties->listManagementWebproperties($firstAccountId);

			if (count($properties->getItems()) > 0) {
				$items = $properties->getItems();
				$firstPropertyId = $items[0]->getId();

				// Get the list of views (profiles) for the authorized user.
				$profiles = $analytics->management_profiles->listManagementProfiles($firstAccountId, $firstPropertyId);

				if (count($profiles->getItems()) > 0) {
					$items = $profiles->getItems();

					// Return the first view (profile) ID.
                    \Cache::forever('firstProfileId',$items[0]->getId());
					return $items[0]->getId();

				} else {
					throw new Exception('No views (profiles) found for this user.');
				}
			} else {
				throw new Exception('No properties found for this user.');
			}
		} else {
			throw new Exception('No accounts found for this user.');
		}
	}

	private static function ParseData($rawData) {
		return $rawData->getRows();
	}

	private static function GetCachedData($analytics, $profileId, $startDate, $endDate, $type, $options) {
		$keyPrefix = $profileId . '_' . $type . '_';

		$carbonStartDate = static::DateToCarbon($startDate);
		$carbonEndDate = static::DateToCarbon($endDate);

		if (@$options['dimensions'] == 'ga:date'){
			$cachedData = [];
			while($carbonStartDate->lte($carbonEndDate)){
				$key = $keyPrefix . $carbonStartDate->format('Ymd');
				$cachedValue = \Cache::get($key);
                if ($cachedValue === "" || $cachedValue === null) {
					break;
				}
				$cachedData[$carbonStartDate->format('Ymd')] = $cachedValue;
				$carbonStartDate->addDays(1)->startOfDay();
			}
			$remainingData = static::GetDataFromGoogle($analytics, $profileId, $carbonStartDate, $carbonEndDate, $type, $options);

			$data = array_merge($cachedData, $remainingData);
		} else {
			$key = $keyPrefix . $carbonStartDate->format('Ymd') . $carbonEndDate->format('Ymd');
			$data = \Cache::get($key);
			if (empty($data)) {
				$data = static::GetDataFromGoogle($analytics, $profileId, $carbonStartDate, $carbonEndDate, $type, $options);
			}
		}
		return $data;
	}

	private static function GetDataFromGoogle($analytics, $profileId, $carbonStartDate, $carbonEndDate, $type, $options) {
		if($carbonStartDate->gt($carbonEndDate)) return [];

		$keyPrefix = $profileId . '_' . $type . '_';
		$rawData = $analytics->data_ga->get(
			'ga:' . $profileId,
			$carbonStartDate->format('Y-m-d'),
			$carbonEndDate->format('Y-m-d'),
			'ga:'. $type,
			$options

		);
		$data = static::ParseData($rawData);

		$cacheTime = 999999999999999;
		if (@$options['dimensions'] == 'ga:date'){
			$sortedData = [];
			foreach($data as $item){
				$key = $keyPrefix . $item[0];

				if (\Carbon::today()->format('Ymd') == $item[0]) $cacheTime = 30;

				\Cache::put($key, $item[1], $cacheTime);
				$sortedData[$item[0]] = $item[1];
			}
			$data = $sortedData;
		} else {
			$key = $keyPrefix . $carbonStartDate->format('Ymd') . $carbonEndDate->format('Ymd');
			if ($carbonEndDate->eq(\Carbon::today()->startOfDay())) $cacheTime = 30;
            if (empty($data)) $data = [];
			\Cache::put($key, $data, $cacheTime);
		}
		return $data;
	}

	private static function DateToCarbon($date) {
		$carbonDate = \Carbon::today()->startOfDay();
		if ($date == 'yesterday') $carbonDate = \Carbon::yesterday()->startOfDay();
		if (Str::contains($date, 'daysAgo')) {
			$date = substr($date, 0, -7);
			$carbonDate = \Carbon::today()->subDays(intval($date))->startOfDay();
		}
		return $carbonDate;
	}


}
