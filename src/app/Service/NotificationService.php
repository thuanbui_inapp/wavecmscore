<?php

namespace App\Service\CMSCore;

use App\Entity\Base\Notification;
use App\Entity\Base\Role;
use App\Entity\Base\User;
use App\Util\Constant;

class NotificationService {
	const STATUS_READ = 'READ';
	const STATUS_UNREAD = 'UNREAD';

	public static function NotifyUser($userId, $object, $message, $url = '', $title = '') {
		if ($userId instanceof User) $userId = $userId->getKey();
		static::CreateNotification($userId, $object, $message, $url, $title);
	}

	public static function NotifyRole($roleCode, $object, $message, $url = '', $title = '') {
		$roles = Role::where('name', $roleCode)->get();
		foreach($roles as $role){
			static::CreateNotification($role->userId, $object, $message, $url, $title);
		}
	}

	public static function ReadNotification($notificationId){
		if (is_string($notificationId) || is_numeric($notificationId)){
			Notification::where('id', $notificationId)->update(['status' => static::STATUS_READ]);
		} else {
			$notificationId->update(['status' => static::STATUS_READ]);
		}
	}

	private static function CreateNotification($userId, $object, $message, $url = '', $title = ''){
		return Notification::create([
			'userId' => $userId,
			'topicId' => $object->getKey(),
			'topicType' => $object->getMorphClass(),
			'topicTable' => $object->getTable(),
			'status' => static::STATUS_UNREAD,
			'url' => $url,
			'title' => $title,
			'message' => $message,
			'notificationDate' => \Carbon::now()
		]);
	}

	public static function MyNotifications(){
		if (empty(\Auth::user())) return [];
		return Notification::where('userId', \Auth::id())->orderBy('notificationDate', 'desc')->get();
	}
	public static function MyUnreadNotifications($limit = 0){
		if (empty(\Auth::user())) return [];
		$query = Notification::where('userId', \Auth::id())->where('status', static::STATUS_UNREAD)->orderBy('notificationDate', 'desc');
		if ($limit > 0) $query->limit($limit);

		return $query->get();
	}
	public static function MyUnreadNotificationCount(){
		if (empty(\Auth::user())) return 0;
		return Notification::where('userId', \Auth::id())->where('status', static::STATUS_UNREAD)->count();
	}
	public static function MyUnreadNotificationLastUpdate(){
		if (empty(\Auth::user())) return \Carbon::now();
		$result = Notification::select('notificationDate')->where('userId', \Auth::id())->where('status', static::STATUS_UNREAD)->orderBy('notificationDate', 'desc')->first();
		return empty($result) ? '' : $result->notificationDate;
	}
	public static function MyReadAll(){
		Notification::where('userId', \Auth::id())->where('status', static::STATUS_UNREAD)->update(['status' => static::STATUS_READ]);
	}
}