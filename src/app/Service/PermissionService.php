<?php
namespace App\Service\CMSCore;

use App\Entity\Base\User; 
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;

class PermissionService {

	const PERMISSION_LIST = 'PERMISSION_LIST'; 

	public static function SaveUserPermission(User $user) {
		$permissions = $user->userPermissions->pluck('name')->toArray(); 
		Cache::forever(self::PERMISSION_LIST . '_' . $user->id, $permissions); 
		return $permissions; 
	} 

	public static function GetUserPermission() {
		$cacheName = self::PERMISSION_LIST . '_' . Auth::id(); 
		$permissions = Cache::get($cacheName); 
		if(empty($permissions)) $permissions = self::SaveUserPermission(Auth::user()); 
		return $permissions; 
	}
}