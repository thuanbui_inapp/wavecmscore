<?php

namespace App\Service\CMSCore;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;


class CRUDService {
	public static function SaveWithData($id, $class) {
		return static::SaveWithDataFromUser($id, $class, Input::all());
	} 

	public static function SaveWithDataFromUser($id, $class, $dataFromUser) {
		$object = $class::get($id);
		$updatedData = static::FormToJson($object->toArray(), $dataFromUser, $object);
		$object->fill($updatedData);
        if ($class::USE_META_SET){
            foreach($class::FORM_META_TYPE as $formName=>$formType){
                $object->$formName = $updatedData[$formName];
            }
        }
		$object->save();
		return $object;
	}

	public static function SaveWithDataCMS($model) {
		$data = Input::all();

		if (count(Config::get('cms.LANGUAGE')) > 1){
			foreach(Config::get('cms.LANGUAGE') as $language){
				if (!isset($data[$language])) continue;

				$json = $model->getContent($language);
				$json = static::FormToJson((array)$json, $data[$language], $model);
				$model->saveContent($language, $json);
			}
		} else {
			$model->json = static::FormToJson((array)$model->json, $data, $model);
		}

		static::addIndexKey($model);
		$model->save();

		return $model;
	}

	public static function FormToJson($json, $data, $model) {

        if (@$model::USE_META_SET){
            foreach($model::FORM_META_TYPE as $formName=>$formType){
                $json[$formName] = static::updateData((array)$json, $data, $formType, $formName, $model);
            }
        }

		foreach($model::FORM_TYPE as $formName=>$formType){
			if (in_array($formName, $model::MANUAL_SAVE_FIELD)) {
				unset($json[$formName]);
				continue;
			}
			$listType = $model::FORM_LIST;
			if (isset($listType[$formName])) continue;

			if ($formType == 'FastSelect'){
                $parentModel = class_basename(get_parent_class($model));

                if ($parentModel != 'Page') {
                    $relation = $model->$formName();
                    $associatedClass = $relation->getRelated();
                    $model->save();
                    if(!isset($data[$formName])){
                        $data[$formName] = [];
                    }
                    if ($relation instanceof  \Illuminate\Database\Eloquent\Relations\BelongsToMany){
                        $model->$formName()->sync( $associatedClass::whereIn('id', $data[$formName] )->get() );
                    }
                }else {
                    $json[$formName] = json_encode($data[$formName]);
                }
			} else if ($formType == 'DateRange' || $formType == 'TimeRange') {
				$json[$formName.'From'] = static::updateData((array)$json, $data, $formType, $formName.'From', $model);
				$json[$formName.'To'] = static::updateData((array)$json, $data, $formType, $formName.'To', $model);
			} else {
				$json[$formName] = static::updateData((array)$json, $data, $formType, $formName, $model);
			}
		}
		foreach($model::FORM_LIST as $formName=>$list){
			if (in_array($formName, $model::MANUAL_SAVE_FIELD)) {
				unset($json[$formName]);
				continue;
			}
			if (count($list) == 0) break;
			if (!isset($data[$formName])) continue;

			$listIndex = 0;

			$updatedjson = [$formName => []];
			while(isset($data[$formName][$listIndex])) {
				if (!isset($json[$formName][$listIndex])) $json[$formName][$listIndex] = [];

				foreach ($list as $listItemFormName => $listItemFormType) {
					$updatedjson[$formName][$listIndex][$listItemFormName] = static::updateData((array)$json[$formName][$listIndex], $data[$formName][$listIndex], $listItemFormType, $listItemFormName, $model);
				}
				$listIndex++;
			}
			$json[$formName] = $updatedjson[$formName];
		}

		return $json;
	}

    public static function useUpdateData($json, $data, $formType, $formName, $model){
        $updatedData = static::updateData($json, $data, $formType, $formName, $model);


        return @$updatedData;
    }

	private static function updateData($json, $data, $formType, $formName, $model){
		$dataKey = array_keys($data);
		if (substr($formType,0,5) == 'Image') {
			if (!$model::IS_CMS && !empty($json[$formName]) && is_string($json[$formName])) $json[$formName] = json_decode($json[$formName]);

			$imageKeys = preg_grep('/'.$formName.'/', $dataKey);
			$updatedImage = [];
			foreach($imageKeys as $imageKey){
				$imageIndex = substr($imageKey, strlen($formName), strlen($imageKey));

				if (!isset($json[$formName])) $json[$formName] = [];

				if (empty($data[$formName.$imageIndex])){
					continue;
				}

				if ($data[$formName.$imageIndex] == 'DELETE_IMAGE'){
					if (isset($json[$formName][$imageIndex]))ImageService::delete( $json[$formName][$imageIndex] );
					$json[$formName][$imageIndex] = '';
				} else if (!is_string($data[$formName.$imageIndex])){
					$json[$formName][$imageIndex] = ImageService::uploadImage($data[$formName.$imageIndex]);
				} else {
					$json[$formName][$imageIndex] = $data[$formName.$imageIndex];
				}

				if (!empty($json[$formName][$imageIndex])) $updatedImage[] = $json[$formName][$imageIndex];
			}
			$json[$formName] = $updatedImage;

			if (!$model::IS_CMS) $json[$formName] = json_encode($json[$formName]);
		} else if (substr($formType,0,4) == 'File'){
            if (!$model::IS_CMS && !empty($json[$formName]) && is_string($json[$formName])) $json[$formName] = json_decode($json[$formName]);

            $fileKeys = preg_grep('/'.$formName.'/', $dataKey);

            foreach($fileKeys as $fileItem) {
                $fileIndex = substr($fileItem, strlen($formName), strlen($fileItem));

                if (isset($data[$formName.$fileIndex]) && (!is_string($data[$formName.$fileIndex]) || $data[$formName.$fileIndex] == 'DELETE_FILE') && !empty($json[$formName])) {
                    FileService::delete($json[$formName][$fileIndex]);
                    $json[$formName] = [];
                }

                if (isset($data[$formName.$fileIndex]) && !is_string($data[$formName.$fileIndex])) {
                    $json[$formName][$fileIndex] = FileService::UploadFile($data[$formName . $fileIndex]);
                } elseif(!isset($data[$formName.$fileIndex])){
                    $json[$formName] = [];
                }
            }

            if (!$model::IS_CMS) $json[$formName] = json_encode($json[$formName]);
        } else {
			if (isset($data[$formName])){
				$json[$formName] = $data[$formName];
				if (($formType == 'Date' || $formType == 'DateRange') && !$model::IS_CMS){
					$json[$formName] = new \Carbon($data[$formName]);
				} else {
					$json[$formName] = $data[$formName];
				}
			}
		}
		return $json[$formName];
	}

	private static function addIndexKey($object){
		if (count($object::INDEX_KEY) == 0) return;

		if (count(Config::get('cms.LANGUAGE')) > 1){
			$language = Config::get('cms.LANGUAGE')[0];
			$json = $object->getContent($language);
			foreach($object::INDEX_KEY as $key=>&$column){
				if (isset($json->$key)) $object->$column = $json->$key;
			}
		} else {
			foreach($object::INDEX_KEY as $key=>&$column){
				if (isset($json->$key)) $object->$column = $object->json->$key;
			}
		}

	}
}