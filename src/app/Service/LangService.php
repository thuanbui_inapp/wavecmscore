<?php
namespace App\Service;


use App\Service\CMSCore\CRUDService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Config;

class LangService {


    public static function saveMultipleLanguageData($class, $id, $dataFromInput, $parentLang){

        $model = [];

        $parentId = 0;

        foreach (Config::get('cms.LANGUAGE') as $index => $lang){
            if ($lang == $parentLang) {
                if (isset($dataFromInput->$lang)){
                    $returnModel = self::saveDataByLang($class, $id, $dataFromInput->$lang, $lang, 0);
                }else {
                    $returnModel = self::saveDataByLang($class, $id, $dataFromInput, $lang, 0);
                }

                $parentId = $returnModel->id;

                $model[$lang] = $returnModel;
            }else {
                $returnModel = self::saveDataByLang($class, $id, $dataFromInput->$lang, $lang, $parentId);
                $model[$lang] = $returnModel;
            }
        }


        return $model;
    }


    public static function saveDataByLang($class, $id, $data, $lang, $parentId = 0){
        $model = $class::get($id);

        if ($parentId) {
            $parentModel = $class::get($parentId);
            $model = $class::where('parentId', $parentModel->id)->get()->first();

            if (!$model) {
                $model = new $class();
            }
        }

        $updatedData = CRUDService::FormToJson($model->toArray(), (array)$data, $model);

        $model->fill($updatedData);

        if ($model::USE_META_SET){
            foreach($model::FORM_META_TYPE as $formName=>$formType){
                $model->$formName = $updatedData[$formName];
            }
        }

        $model->lang = $lang;

        if ($parentId) {
            $model->parentId = $parentId;
        }

        $model->save();

        return $model;
    }


    public static function getViewLangData($class, $proccessData){
        $selectedLang = getSelectedLang();

        if (is_array($proccessData) || $proccessData instanceof Collection){
            foreach ($proccessData as $key => $item){
                if ($item->lang != $selectedLang){
                    $selectedLangData = $class::where('parentId', $item->id)->where('lang', $selectedLang)->get()->first();

                    $item->selectedLangData = $selectedLangData;
                }else {
                    $item->selectedLangData = $item;
                }
            }
        }else {
            if ($proccessData->lang != $selectedLang){
                $selectedLangData = $class::where('parentId', $proccessData->id)->where('lang', $selectedLang)->get()->first();

                $proccessData->selectedLangData = $selectedLangData;
            }else {
                $proccessData->selectedLangData = $proccessData;
            }
        }


        return $proccessData;
    }
}
