<?php

namespace App\Service\CMSCore;

class ComponentService {
	public static function AutoCompleteSearch($class) {

		$q = \Input::get('q');
		if (empty($q)) $q = '';

		$limit = \Input::get('limit');
		if (empty($limit) || !Is_Numeric($limit)) $limit = 10;

		$list = $class::where('name', 'like', '%'.$q.'%')->select('id', 'name')->orderBy('name')->limit($limit)->get();
		return $list;
	}

}