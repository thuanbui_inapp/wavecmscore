<?php

namespace App\Service\CMSCore;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;


class URLService {
	public static function GenerateBackUrl() {
		$backButtonHistoryUrls = Session::get('backButtonHistoryUrls');

		if (empty($backButtonHistoryUrls)) $backButtonHistoryUrls = [];

		$previousUrl = URL::previous();
		if ($previousUrl != route('admin.back') && static::GetLastSessionHistoryUrl() != $previousUrl) $backButtonHistoryUrls[] = $previousUrl;

		if(!Session::get('thisisbackaction')){
			Session::put('backButtonHistoryUrls', $backButtonHistoryUrls);
		} else {
			Session::put('thisisbackaction', false);
		}

		return route('admin.back');
	}

	public static function DeleteLastBackUrl() {
		$backButtonHistoryUrls = Session::get('backButtonHistoryUrls');

		if (empty($backButtonHistoryUrls) || count($backButtonHistoryUrls) == 0) return route('admin.home');

		$lastBackUrl = $backButtonHistoryUrls[ count($backButtonHistoryUrls) -1 ];
		array_pop($backButtonHistoryUrls);
		Session::put('backButtonHistoryUrls', $backButtonHistoryUrls);

		Session::put('thisisbackaction', true);

		return $lastBackUrl;
	}

	private static function GetLastSessionHistoryUrl() {
		$backButtonHistoryUrls = Session::get('backButtonHistoryUrls');

		if (empty($backButtonHistoryUrls) || count($backButtonHistoryUrls) == 0) return '';

		return $backButtonHistoryUrls[ count($backButtonHistoryUrls) -1 ];;
	}

}