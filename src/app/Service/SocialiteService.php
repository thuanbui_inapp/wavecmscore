<?php

namespace App\Service\CMSCore;

use App\Entity\Base\Role;
use App\Entity\Base\User;
use App\Entity\User\UserBO;
use GuzzleHttp\Exception\RequestException;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Two\InvalidStateException;

class SocialiteService {
    public static function Redirect($provider){
        if (!config('cms.GoogleSignIn')) return redirect()->to('/admin/login');
        $driver = Socialite::driver($provider);
        $driver->redirectUrl( route('admin.auth.socialiteHandle', ['provider' => $provider]) );
        return $driver->redirect();
    }

    public static function Handle($provider){
        if (!config('cms.GoogleSignIn')) return redirect()->to('/admin/login');
        $driver = Socialite::driver($provider);
        $driver->redirectUrl( route('admin.auth.socialiteHandle', ['provider' => $provider]) );

        if (\Input::get('denied') != '') return (object)['status' => false, 'message' => 'You have denied the authorization to sign in with Google.'];

        try {
            $socialiteUser = $driver->user();
        } catch (RequestException $e) {
            \Log::error($e);
            return (object)['status' => false, 'message' => $provider. ' authorization was unsuccessful.'];
        } catch (InvalidStateException $e) {
            \Log::error($e);
            return (object)['status' => false, 'message' => $provider. ' authorization was unsuccessful.'];
        }

        $email = $socialiteUser->email;

        $user = User::where('email', $socialiteUser->email)->first();

        if (!empty(env('GOOGLE_SIGNIN_DOMAIN'))) {
            $emails = explode(',',env('GOOGLE_SIGNIN_DOMAIN'));
            $emailDomain = explode('@',$email);
            if (!in_array($emailDomain[1], $emails) && empty($user)) return (object)['status' => false, 'message' => 'Invalid user'];
        }

        if (empty($user)) {
            $user = User::create([
                'email' => $socialiteUser->email,
                'name' => $socialiteUser->name,
            ]);

            Role::create([
                'userId' => $user->id,
                'name' => 'ADMIN',
            ]);
        }

        \Auth::login($user);

        if (config('cms.acl') && count($user->userPermissions) === 0) return (object)['status' => true, 'message' => 'No permission'];

        return (object)['status' => true, 'message' => ''];
    }
}