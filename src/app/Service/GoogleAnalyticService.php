<?php
namespace App\Service;



use App\Entity\City;
use App\Entity\Province;
use App\Entity\Subdistrict;
use App\Entity\Village;
use Illuminate\Support\Facades\Cache;

class GoogleAnalyticService {
	public static function GetSessions($startDate, $endDate){
		return GoogleAnalyticCoreService::GetData($startDate, $endDate, 'sessions', [ 'dimensions' => 'ga:date']);
	}
	public static function GetUsers($startDate, $endDate){
		return GoogleAnalyticCoreService::GetData($startDate, $endDate, 'users', [ 'dimensions' => 'ga:date']);
	}
	public static function GetPageVisit($startDate, $endDate, $limit = 0){
		$data = GoogleAnalyticCoreService::GetData($startDate, $endDate, 'pageViews', [ 'dimensions' => 'ga:pagePath']);
		$data = static::AppendEmpty($data, $limit);
		return $data;
	}

	public static function GetAcquisition($startDate, $endDate, $limit = 0){
		$data = GoogleAnalyticCoreService::GetData($startDate, $endDate, 'users', [ 'dimensions' => 'ga:channelGrouping']);
		$data = static::AppendEmpty($data, $limit);
		return $data;
	}

	private static function AppendEmpty($data, $limit){

	    $data = !empty($data) ? $data : [];

		if (count($data) < $limit) {
			$emptyRecordsCount = $limit - count($data);

			for($i = 0; $i < $emptyRecordsCount; $i++){
				$emptyRecord = [];
				$emptyRecord[0] = '&nbsp;';
				$emptyRecord[1] = '&nbsp;';

				array_push($data, $emptyRecord);
			}
		}

		usort($data, function($a, $b){
			return $a[1] < $b[1];
		});

		if ($limit > 0) $data = array_slice($data, 0, $limit);
		return $data;
	}

}
