<?php

namespace App\Scopes\CMSCore;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class UserRoleScope implements Scope {
    protected $roles;

    public function __construct($roles) {
        if (is_string($roles)) {
            $this->roles = array($roles);
        } else {
            $this->roles = $roles;
        }
    }

    public function apply(Builder $builder, Model $model) {
        $builder->whereHas('roles', function ($q) {
            $q->whereIn('name', $this->roles);
        });
    }
}