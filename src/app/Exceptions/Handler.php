<?php

namespace App\Exceptions\CMSCore;

use App\Util\CMSCore\ResponseUtil;
use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
		if (env('APP_ENV') == 'production') return response()->view('cms.errors.500');

        if ($e instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
        	if ($request->path() == 'admin/notification/my/list' || $request->path() == 'admin/notification/my/count'){
				//echo json_encode(ResponseUtil::Error('CMSCore::Notification() is not set in routes.php'));
				return response()->view('cms.errors.500', ['json' => ResponseUtil::Error('CMSCore::Notification() is not set in routes.php')]);
			}

            return redirect('/');
        }
        return parent::render($request, $e);
    }
}
