<?php

namespace App\Http\Controllers\CMSCore\Admin;

use App\Entity\Base\Role;
use App\Entity\Base\User;
use App\Http\Controllers\CMSCore\Controller;
use App\Service\CMSCore\URLService;
use App\Service\GoogleAnalyticService;
use App\Util\CMSCore\CodingConstant;
use App\Util\CMSCore\ResponseUtil;

class GoogleAnalyticController extends Controller {
	public function ajaxSessions() {
        $input = inputObject();

        $dateFrom = '7daysAgo';
        $dateTo = 'today';

        if (isset($input->dateFrom) && !empty($input->dateFrom)) {
            $dateFrom = $input->dateFrom;
        }

        if (isset($input->dateTo) && !empty($input->dateTo)){
            $dateTo = $input->dateTo;
        }

        return ResponseUtil::Success([ GoogleAnalyticService::GetSessions($dateFrom, $dateTo) ]);
	}
	public function ajaxUsers() {
        $input = inputObject();

        $dateFrom = '7daysAgo';
        $dateTo = 'today';

        if (isset($input->dateFrom) && !empty($input->dateFrom)) {
            $dateFrom = $input->dateFrom;
        }

        if (isset($input->dateTo) && !empty($input->dateTo)){
            $dateTo = $input->dateTo;
        }

        return ResponseUtil::Success([ GoogleAnalyticService::GetUsers($dateFrom, $dateTo) ]);
	}
}
