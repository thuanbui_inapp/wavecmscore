<?php

namespace App\Http\Controllers\CMSCore\Admin;

use App\Entity\Base\Notification;
use App\Entity\Base\Role;
use App\Entity\Base\User;
use App\Http\Controllers\CMSCore\Controller;
use App\Service\CMSCore\NotificationService;
use App\Service\CMSCore\URLService;
use App\Util\CMSCore\CodingConstant;
use App\Util\CMSCore\ResponseUtil;

class NotificationController extends Controller {
	public function index() {
		return view('cms.feature.notification.index',['list' => NotificationService::MyUnreadNotifications(), 'model' => Notification::class]);
	}

	public function badge($type) {
		if ($type != 'full') $type == 'summary';

		$response = [
			'count' => NotificationService::MyUnreadNotificationCount(),
			'lastUpdate' => NotificationService::MyUnreadNotificationLastUpdate()
		];

		if ($type == 'full') $response['list'] = NotificationService::MyUnreadNotifications(5);
		return ResponseUtil::Success( $response );
	}
	public function topic($notificationId) {
		$notification = Notification::find($notificationId);
		if (empty($notification) || empty($notification->url)) return redirect()->route('admin.home');

		NotificationService::ReadNotification($notificationId);
		return redirect()->to($notification->url);
	}
	public function readAll() {
		NotificationService::MyReadAll();
		return ResponseUtil::Success( 'Success' );
	}
}
