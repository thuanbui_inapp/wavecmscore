<?php

namespace App\Http\Controllers\CMSCore\Admin;

use App\Entity\Base\Role;
use App\Entity\Base\User;
use App\Http\Controllers\CMSCore\Controller;
use App\Util\CMSCore\CodingConstant;

class UserController extends Controller {
	public function index() {
		return view('cms.default.index', ['list' => User::all(), 'model' => '\App\Entity\Base\User']);
	}

	public function details($id = -1) {
		if ($id == -1) $user = \Auth::user(); else {
			if (!\Auth::isAdmin()) $user = \Auth::user(); else $user = User::get($id);
		}
		return view('cms.user.details', ['model' => $user]);
	}

	public function save($id) {
		$data = \Input::all();
		$user = \Auth::user();
		if (empty($id)) {
			$user = User::where('email', $data['email'])->get();
			if (count($user) > 0) return redirect()->back()->withErrors(['email' => 'User with that email address already exist']);
		}

		if ($id == -1) {
			$user = \Auth::user();
			$user = User::SaveWithData($user->id);
		} else {
			if (!\Auth::isAdmin()) return redirect('/users');
			$user = User::SaveWithData($id);
		}

		if (empty($id)) {
			$user->status = User::STATUS_ACTIVE;
		}

		if (!empty($data['password'])) {
			$user->password = \Hash::make($data['password']);
		}
		$user->save();

		foreach ($user->roles as $role) {
			if (!in_array($role->name, $data['roles'])) $role->delete();
		};
		foreach ($data['roles'] as $newrole) {
			$exist = false;
			foreach ($user->roles as $role) {
				if ($role->name == $newrole) $exist = true;
			};
			if (!$exist) {
				$newUserRole = new Role(['name' => $newrole, CodingConstant::ConvertCase('user_id') => $user->id,]);
				$newUserRole->save();
			}
		};

		return redirect('/admin/users');
		/*}
		return redirect('/');*/
	}

}
