<?php

namespace App\Http\Controllers\CMSCore\Admin;

use App\Http\Controllers\CMSCore\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class CMSController extends Controller {

    public function index($type, $subtype = '') {
        $typeEntity = nameToBaseEntity($type);
        if (!class_exists($typeEntity)) return redirect ('/');

        if ($typeEntity::ALLOW_MULTIPLE){
            $subtypeEntity = nameToEntity($subtype);
            if (!class_exists($subtypeEntity)) return redirect ('/');

            $list = $subtypeEntity::where(['type'=>$type, 'subtype'=>$subtype])->get();
            $typeEntity = $subtypeEntity;
        } else {
            $list = [];
            $pageList = Config::get('cms.Pages');
            foreach($pageList as $entity){
                $subentity = nameToEntity($entity);
                $list[] = new $subentity();
            }
        }

        return view('cms.cmspage.index', ['type' => $type, 'typeEntity' => $typeEntity, 'subtype' => $subtype, 'list'=>$list]);
    }

    public function details($type, $subtype, $id = -1) {
        $typeEntity = nameToBaseEntity($type);
        if (!class_exists($typeEntity)) return redirect ('/');

        $subtypeEntity = nameToEntity($subtype);
        if (!class_exists($subtypeEntity) || get_parent_class($subtypeEntity) != $typeEntity) return redirect ('/');

        if ($typeEntity::ALLOW_MULTIPLE){
            $model = $subtypeEntity::get($id);
        } else {
            $model = $subtypeEntity::where(['type'=>$type, 'subtype'=>$subtype])->get()->first();
            if (empty($model)) $model = new $subtypeEntity();
        }

        return view('cms.cmspage.details', ['type' => $type, 'typeEntity' => $typeEntity, 'subtype' => $subtype, 'subtypeEntity' => $subtypeEntity, 'model' => $model]);
    }

    public function save($type, $subtype, $id = -1) {
        $typeEntity = nameToBaseEntity($type);
        if (!class_exists($typeEntity)) return redirect ('/');

        $subtypeEntity = nameToEntity($subtype);
        if (!class_exists($subtypeEntity) || get_parent_class($subtypeEntity) != $typeEntity) return redirect ('/');

        $redirect = '';
        if ($typeEntity::ALLOW_MULTIPLE){
            $model = $subtypeEntity::get($id);
            $model->type = $type;
            $model->subtype = $subtype;
            $model->save();

            $redirect = '/'.$subtype;
        } else {
            $model = $subtypeEntity::where(['type'=>$type, 'subtype'=>$subtype])->get()->first();
            if (empty($model)) {
                $model = new $subtypeEntity();
                $model->type = $type;
                $model->subtype = $subtype;
                $model->save();
            }
        }
        $subtypeEntity::SaveWithDataCMS($model);

        if (empty($id)) $model->postCreateAction();
        else $model->postUpdateAction();
        return redirect('/admin/cms/'.$type .$redirect);
        //return redirect()->back();
    }

    public function delete($type, $subtype, $id = -1) {
        $typeEntity = nameToBaseEntity($type);
        if (!class_exists($typeEntity)) return redirect ('/');

        $subtypeEntity = nameToEntity($subtype);
        if (!class_exists($subtypeEntity) || get_parent_class($subtypeEntity) != $typeEntity) return redirect ('/');

        $redirect = '';
        if ($subtypeEntity::ALLOW_DELETION){
            $model = $subtypeEntity::get($id);
            if (!empty($model)){
	            $model->delete();
	            $model->postDeleteAction();
            }
            $redirect = '/'.$subtype;
        }
        return redirect('/admin/cms/'.$type .$redirect);
    }


}
