<?php

namespace App\Http\Controllers\CMSCore\Admin;

use App\Http\Controllers\CMSCore\Controller;
use App\Service\CMSCore\SocialiteService;

class AuthController extends Controller {

    public function socialRedirect($provider){
        return SocialiteService::Redirect($provider);
    }

    public function socialHandle($provider){
        $handle = SocialiteService::Handle($provider);

        if (!$handle->status) {
            return redirect()->to('/admin/login')->with('socialError', $handle->message);
        } elseif ($handle->status && $handle->message === 'No permission') {
            return redirect()->route('admin.auth.nopermission');
        }

        return redirect()->to('/admin');
    }

    public function noPermission(){
        return view('cms.auth.nopermission');
    }

    public function unauthorized(){
        return view('cms.auth.unauthorized');
    }
}
