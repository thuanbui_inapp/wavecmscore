<?php

namespace App\Http\Controllers\CMSCore\Admin;

use App\Entity\Base\Role;
use App\Entity\Base\User;
use App\Http\Controllers\CMSCore\Controller;
use App\Service\CMSCore\URLService;
use App\Util\CMSCore\CodingConstant;

class BackController extends Controller {
	public function back() {
		return redirect(URLService::DeleteLastBackUrl());
	}
}
