<?php

namespace App\Http\Middleware\CMSCore;

use Closure;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class CheckPermission
{
    public function handle($request, Closure $next){
        // Check if project uses ACL
        if(config('cms.acl') !== true) return $next($request);

        // Get Required permissions from route
        $permissions = $this->getRequiredPermissions($request->route());
        if (empty($permissions)) return $next($request);
        if (empty($request->user())) return redirect( $this->GetRedirectForRoute($request->route()) );

        // Check User Permissions
        if(!checkPermission($permissions)) {
            if(view()->exists('errors.401')) abort(401, 'Unauthorized');
            else return response()->view('cms.errors.401');
        }
        return $next($request);
    }

    private function getRequiredPermissions($route){
        $actions = $route->getAction();
        return isset($actions['permission']) ? $actions['permission'] : null;
    }
}
