<?php

namespace App\Http\Middleware\CMSCore;

use App\Util\CMSCore\Response;
use Closure;

class AuthenticateAPI
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null){
        if(\Auth::check()) return $next($request);
        try {
            $isStrict = $this->isStrict($request->route()); 
            $token = \JWTAuth::getToken();
            if($token){
                \JWTAuth::parseToken()->authenticate();
            } elseif($isStrict == true) {
                Response::unauthorized('Session Unavailable');
            }
        }
        catch (TokenInvalidException $e) {
            Response::unauthorized('Session Unavailable');
        }
        catch (TokenExpiredException $e) {
            Response::unauthorized('Session Unavailable');
        }
        catch (JWTException $e) {
            Response::unauthorized('Session Unavailable');
        }
        return $next($request);
    } 

    private function isStrict($route) {
        $actions = $route->getAction();
        return isset($actions['strict']) ? $actions['strict'] : true;
    }
}
