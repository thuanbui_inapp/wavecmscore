<?php

namespace App\Http\Middleware\CMSCore;

use App\Util\CMSCore\Response;
use Closure;

class CheckAPIKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null){
        // Get Required roles from route
        $apiKeyType = $this->GetApiKeyTypeForRoute($request->route());
        if (empty($apiKeyType)) Response::unauthorized('Unauthorized Access');

        $apiKey = env($apiKeyType);
        if (empty($apiKey)) Response::unauthorized('Unauthorized Access');

        $userApiKey = $request->header('apiKey');
        if (empty($userApiKey) || $apiKey != $userApiKey) Response::unauthorized('Unauthorized Access');

        return $next($request);
    }

    private function GetApiKeyTypeForRoute($route){
        $actions = $route->getAction();
        return isset($actions['apiKey']) ? $actions['apiKey'] : '';
    }

}
