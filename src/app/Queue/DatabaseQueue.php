<?php

namespace App\Queue;

use Carbon\Carbon;
use Illuminate\Queue\DatabaseQueue as DefaultDatabaseQueue; 

class DatabaseQueue extends DefaultDatabaseQueue 
{
    protected function isAvailable($query)
    {
        $query->where(function ($query) {
            $query->where('reserved', 0);
            $query->where('availableAt', '<=', $this->getTime());
        });
    }

    protected function isReservedButExpired($query)
    {
        $expiration = Carbon::now()->subSeconds($this->expire)->getTimestamp();

        $query->orWhere(function ($query) use ($expiration) {
            $query->where('reserved', 1);
            $query->where('reservedAt', '<=', $expiration);
        });
    }

    protected function markJobAsReserved($job)
    {
        $job->reserved = 1;
        $job->attempts = $job->attempts + 1;
        $job->reserved_at = $this->getTime();

        $this->database->table($this->table)->where('id', $job->id)->update([
            'reserved' => $job->reserved,
            'reservedAt' => $job->reserved_at,
            'attempts' => $job->attempts,
        ]);

        return $job;
    }

    protected function buildDatabaseRecord($queue, $payload, $availableAt, $attempts = 0)
    {
        return [
            'queue' => $queue,
            'payload' => $payload,
            'attempts' => $attempts,
            'reserved' => 0,
            'reservedAt' => null,
            'availableAt' => $availableAt,
            'createdAt' => $this->getTime()
        ];
    }
}
