<?php
namespace App\Queue; 

use App\Queue\DatabaseQueue; 
use Illuminate\Queue\Connectors\DatabaseConnector as DefaultDatabaseConnector;
use Illuminate\Support\Arr;

class DatabaseConnector extends DefaultDatabaseConnector {
	public function connect(array $config)
    {
        return new DatabaseQueue(
            $this->connections->connection(Arr::get($config, 'connection')),
            $config['table'],
            $config['queue'],
            Arr::get($config, 'expire', 60)
        );
    }
}