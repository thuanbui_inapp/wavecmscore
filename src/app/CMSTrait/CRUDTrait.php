<?php

namespace App\CMSTrait;

use App\Util\CMSCore\CodingConstant;

trait CRUDTrait {

	protected static function boot() {
		parent::boot();

		$createdBy = CodingConstant::ConvertCase('createdBy');
		$updatedBy = CodingConstant::ConvertCase('updatedBy');

		static::saving(function($table) use($updatedBy){
			if (\Auth::user() != null) {
				$table->$updatedBy = \Auth::user()->id;
			}
			else {
				$table->$updatedBy = (int) env('SYSTEM_USER_ID', '0');
			}

			if (config('cms.NoZeroDate')){
				foreach ($table->attributes as $key => $value) {
					$table->{$key} = empty($value) ? null : $value;
				}
			}
		});
		static::creating(function($table) use($createdBy, $updatedBy){
			if (\Auth::user() != null) {
				$table->$createdBy = \Auth::user()->id;
				$table->$updatedBy = \Auth::user()->id;
			}
			else {
				$table->$createdBy = (int) env('SYSTEM_USER_ID', '0');
				$table->$updatedBy = (int) env('SYSTEM_USER_ID', '0');
			}

			// if (config('cms.NoZeroDate')){
			// 	foreach ($table->attributes as $key => $value) {
			// 		$table->{$key} = empty($value) ? null : $value;
			// 	}
			// }
		});
	}
	protected function runSoftDelete(){
		$deletedBy = CodingConstant::ConvertCase('deletedBy');

		$query = $this->newQueryWithoutScopes()->where($this->getKeyName(), $this->getKey());
		$this->{$this->getDeletedAtColumn()} = $time = $this->freshTimestamp();

		$updateColumn = [$this->getDeletedAtColumn() => $this->fromDateTime($time)];
		if (\Auth::user() != null) {
			$updateColumn[$deletedBy] = \Auth::user()->id;
		}
		else {
			$updateColumn[$deletedBy] = (int) env('SYSTEM_USER_ID', '0');
		}
		$query->update($updateColumn);
	}

}
